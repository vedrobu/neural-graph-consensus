import matplotlib.pyplot as plt
import numpy as np
from neural_wrappers.utilities import npGetInfo

from cycleconcepts.nodes import *
from cycleconcepts.plot_utils import toImageFuncs

# Update Str->Node as keys
toImageFuncs = {
	"rgb" : toImageFuncs[RGB],
	"rgbDomain2" : toImageFuncs[RGB],
	"neighbour" : toImageFuncs[RGB],
	"depth" : toImageFuncs[Depth],
	"pose_quat" : toImageFuncs[Pose],
	"semantic_segmentation" : toImageFuncs[Semantic],
	"normal" : toImageFuncs[Normal],
	"wireframeregression" : toImageFuncs[WireframeRegression],
	"cameranormal" : toImageFuncs[CameraNormal],
	"halftone" : toImageFuncs[Halftone],
}

def npGetInfoData(x, depth=1):
	for k in x:
		if type(x[k]) is dict:
			print("%s- %s :" % (depth * "  ", k))
			npGetInfoData(x[k], depth+1)
		else:
			print("%s- %s : %s" % (depth * "  ", k, npGetInfo(x[k])))

def plotOneItem(ax, data):
	[axi.set_axis_off() for axi in ax.ravel()]
	for i, k in enumerate(data):
		# print(k, data[k].min(), data[k].max(), data[k].dtype)
		ax[i].imshow(data[k])
		ax[i].set_title(k, fontsize=30)

def test_dataset(generator):
	cnt = 0

	titles = {
		"rgb" : "RGB",
		"rgbDomain2" : "RGB\n(Domain 2)",
		"neighbour" : "RGB\nNeighbour",
		"depth" : "Depth",
		"pose" : "Pose",
		"semantic_segmentation" : "Semantic\nSegmentation",
		"normal" : "Normals",
		"optical_flow" : "Optical Flow",
		"wireframe" : "Wireframe",
		"cameranormal" : "Camera\nNormal",
		"halftone" : "Halftone",
	}

	for (data, _), B in generator:
		MB = data["rgb"].shape[0]
		npGetInfoData(data)
		keys = ["rgb", "rgbDomain2", "depth", "pose_quat", "semantic_segmentation", \
			"normal", "optical_flow", "wireframe", "cameranormal", "halftone", "neighbour", \
			"position_dot_translation_only"]
		keys = list(filter(lambda x : x in data.keys(), keys))

		for i in range(MB):
			cnt += 1
			arr = plt.subplots(1, len(keys))[1]
			# Kind of convoluted. Match titles above with functions of toImageFuncs, given the actual GT keys,
			#  given also above.

			thisImgs = [toImageFuncs[key](data[key][i]) for key in keys]
			thisTitles = [titles[key] for key in keys]
			items = dict(zip(thisTitles, thisImgs))

			plotOneItem(arr, items)

			# for j in range(numNeightbours):
			# 	items = dict(zip(map(lambda x : "Neighbour %d %s" % (j, x), titles), \
			# 		[toImageFuncs[key](data["neighbours"][key][i][j]) for key in keys]))
			# 	plotOneItem(arr[j + 1], items)

			plt.gcf().set_size_inches(5 * len(keys), 2 * len(keys))
			# plt.show()
			plt.savefig("%d.png" % (cnt))
			plt.close()