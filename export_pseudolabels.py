import numpy as np
import torch as tr
from tqdm import tqdm
from neural_wrappers.pytorch import trGetData

from cycleconcepts.utils import makeNpy

def export_pseudolabels(model, generator, numSteps):
	model.eval()
	targetEdge = model.edges[-1]
	targetNode = targetEdge.outputNode
	cnt = 0

	for i in tqdm(range(numSteps)):
		data = next(generator)[0]
		trLabels = trGetData(data)
		for node in model.nodes:
			node.messages = {}
			if node.groundTruthKey in trLabels:
				node.setGroundTruth(trLabels)

		with tr.no_grad():
			results = model.npForward(data)
		targetNodeOutput = results[str(targetEdge)][0]
		MB = len(targetNodeOutput)

		for j in range(MB):
			thisNpy = makeNpy(targetNodeOutput[j], targetNode)
			np.save("%d.npy" % cnt, thisNpy)
			cnt += 1
