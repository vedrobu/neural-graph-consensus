# Given an unsupervised dataset with RGB only representations (as NPY), generate dummy NPY files for all the other
#  representations: depth, semantic, etc.
# This is needed so the script for generating the H5 Paths generates all necessary keys, which are to be updated by
#  pseudolabels from graph-ensembles.
# Usage: python generate_dummy_npy.py /path/to/rgb_only_dataset
import numpy as np
import os
from pathlib import Path
from argparse import ArgumentParser
from tqdm import tqdm

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("baseDirectory")
    parser.add_argument("--base_key", default="_rgb_")
    parser.add_argument("--new_keys", default="_cameranormal_,_depth_,_flowr2_,_flowr3_,_flowx1_,_flowx2_,_flowx3_,_flowx4_,_flowy1_,_flowy2_,_flowy3_,_flowy4_,_halftone_,_normal_,_rgbDomain2_,_semanticSegmentation_,_wireframe_")
    args = parser.parse_args()
    args.new_keys = args.new_keys.split(",")
    assert len(args.new_keys) > 0
    return args

def main():
    args = getArgs()
    p = Path(args.baseDirectory).glob("*%s*" % args.base_key)
    files = list(map(lambda x : str(x), filter(lambda x : x.is_file(), p)))
    N = len(files)

    print("Found %d files (key %s). Generating %d dummy files (%d keys)" % (N, args.base_key, \
        N * len(args.new_keys), len(args.new_keys)))
    for i in tqdm(range(N)):
        filePath = files[i]
        for newKey in args.new_keys:
            replacedPath = filePath.replace(args.base_key, newKey)
            os.mknod(replacedPath)
    print("Done!")

if __name__ == "__main__":
    main()