# Script that takes two H5 datasets:
#  - One train + validation dataset
#  - One train only dataset (semi supervised dataset)
#  - The statistics of the two datasets must be identical (positionExtremes and such)
#  - The baseDirectories of the two datasets must be different
# It creates a new H5 dataset with the following properties
# - The new train key is the combination of dataset 1's train and dataset 2's train keys
# - The new train key is combined using a random seed
# - The new validation key is the original validation from dataset 1
# - The new baseDirectory is the intersection of the two baseDirectories
# - The new paths will be of type: newBaseDirectory + (oldDir1 or oldDir2)/item.npy
# The script can be recursively applied to multiple iterations
import h5py
import numpy as np
from argparse import ArgumentParser
from pathlib import Path

from neural_wrappers.utilities import deepCheckEqual, h5ReadDict, h5StoreDict

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("trainValDataset")
	parser.add_argument("trainOnlyDataset")
	parser.add_argument("resultDataset")
	parser.add_argument("--seed", default=42)

	args = parser.parse_args()
	assert Path(args.trainValDataset).is_file()
	assert Path(args.trainOnlyDataset).is_file()
	assert not Path(args.resultDataset).exists()
	return args

# Compute the intersection of the two baseDirectories:
#  baseDirectory1: /scratch/nvme0n1/Datasets/Carla/final_dataset/train_set_256_npy and
#  baseDirectory2: /scratch/nvme0n1/Datasets/Carla/final_dataset/semisupervised_set_1_256_npy
# => newBaseDirectory: /scratch/nvme0n1/Datasets/Carla/final_dataset/
# => suffix1: train_set_256_npy
# => suffix2: semisupervised_set_1_256_npy
def computeNewBaseDirectory(file1, file2):
	baseDirectory1 = file1["others"]["baseDirectory"][()]
	baseDirectory2 = file2["others"]["baseDirectory"][()]
	i = 0
	while True:
		if i == len(baseDirectory1) or i == len(baseDirectory2) or baseDirectory1[i] != baseDirectory2[i]:
			break
		i += 1
	newBaseDirectory = baseDirectory1[0 : i]
	suffix1 = baseDirectory1[i :]
	suffix2 = baseDirectory2[i :]
	return newBaseDirectory, suffix1, suffix2

def main():
	args = getArgs()

	file1 = h5py.File(args.trainValDataset, "r")
	file2 = h5py.File(args.trainOnlyDataset, "r")
	file = h5py.File(args.resultDataset, "w")
	stats1 = h5ReadDict(file1["others"]["dataStatistics"])
	stats2 = h5ReadDict(file2["others"]["dataStatistics"])
	assert deepCheckEqual(stats1, stats2)
	assert file1["train"]["rgb"][0] != file1["validation"]["rgb"][0]
	assert deepCheckEqual(h5ReadDict(file2["train"]), h5ReadDict(file2["validation"]))
	# assert "position_gt" in file2["train"].keys(), "Call merge_pose.py first to set up the pseudolabels " + \
		# "for semisupervised datasetset"

	newBaseDirectory, suffix1, suffix2 = computeNewBaseDirectory(file1, file2)
	train1 = h5ReadDict(file1["train"])
	validation1 = h5ReadDict(file1["validation"])
	train2 = h5ReadDict(file2["train"])
	Keys1 = list(train1.keys())
	Keys2 = list(train2.keys())
	# We store the original GT (they can also be inferred from paths though) positions. However, we drop them here.
	if "position_gt" in Keys2:
		Keys2.remove("position_gt")
	assert Keys1 == Keys2, "%s vs %s" % (Keys1, Keys2)

	nItems1 = len(file1["train"][Keys1[0]])
	nItems2 = len(file2["train"][Keys1[0]])
	nValItems1 = len(file1["validation"][Keys1[0]])
	print("Merging %d train items and %d validation items with %d items (on %d keys)" \
		% (nItems1, nValItems1, nItems2, len(Keys1)))

	file.create_group("train")
	file.create_group("validation")
	file.create_group("others")

	sortedTrainIds1 = np.argsort(train1["ids"][()])
	# Second (semi supervised dataset) is not randomized!
	assert deepCheckEqual(train2["ids"][()], np.sort(train2["ids"][()]))
	np.random.seed(args.seed)
	perm = np.random.permutation(nItems1 + nItems2)
	for Key in Keys1:
		if Key == "ids":
			continue
		dataTrain1 = train1[Key][()]
		dataValidation1 = validation1[Key][()]
		dataTrain2 = train2[Key][()]

		if Key != "position":
			dataTrain1 = map(lambda x : str(x, "utf8"), dataTrain1)
			dataValidation1 = map(lambda x : str(x, "utf8"), dataValidation1)
			dataTrain2 = map(lambda x : str(x, "utf8"), dataTrain2)

			dataTrain1 = map(lambda x : "%s/%s" % (suffix1, x), dataTrain1)
			dataValidation1 = map(lambda x : "%s/%s" % (suffix1, x), dataValidation1)
			dataTrain2 = map(lambda x : "%s/%s" % (suffix2, x), dataTrain2)

			dataTrain1 = np.array(list(dataTrain1), "S")
			dataValidation1 = np.array(list(dataValidation1), "S")
			dataTrain2 = np.array(list(dataTrain2), "S")

		# Restore original order (before first randomization)
		dataTrain1 = dataTrain1[sortedTrainIds1]

		dataTrainCommon = np.concatenate([dataTrain1, dataTrain2])
		dataTrainCommon = dataTrainCommon[perm]

		file["train"][Key] = dataTrainCommon
		file["validation"][Key] = dataValidation1
		print("Key %s. New train shape: %s. New val shape: %s" \
			% (Key, file["train"][Key].shape, file["validation"][Key].shape))
	file["train"]["ids"] = perm
	file["validation"]["ids"] = validation1["ids"]
	h5StoreDict(file["others"], {"baseDirectory" : newBaseDirectory, "dataStatistics" : stats1})
	print("Done! Stored to %s" % args.resultDataset)

if __name__ == "__main__":
	main()