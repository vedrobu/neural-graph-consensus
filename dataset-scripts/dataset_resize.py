import os
import numpy as np
from datetime import datetime
from argparse import ArgumentParser
from tqdm import tqdm
from pathlib import Path

from neural_wrappers.utilities import npGetInfo, npCloseEnough
from media_processing_lib.image import tryReadImage, tryWriteImage
from media_processing_lib.image.resize import resize_skimage, resize_opencv
from neural_wrappers.readers.datasets.carla.utils import unrealPngFromFloat, unrealFloatFromPng

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("baseDirectory")
	parser.add_argument("resultDirectory")
	parser.add_argument("mode")
	parser.add_argument("--desiredShape", default="256,256")
	parser.add_argument("--batchSize", default=100, type=int)
	args = parser.parse_args()

	assert args.mode in ("images", "npy")
	args.desiredShape = list(map(lambda x : int(x), args.desiredShape.split(",")))
	args.baseDirectory = os.path.abspath(args.baseDirectory)
	args.resultDirectory = os.path.abspath(args.resultDirectory)
	return args

def doResizeNpy(img, inPath, height, width):
	if "semantic" in inPath:
		imgResized = resize_opencv(img, width=width, height=height, interpolation="nearest")
	elif ("depth" in inPath) or ("flow" in inPath):
		decodedImg = unrealFloatFromPng(img)
		imgResized = resize_skimage(decodedImg, width=width, height=height, interpolation="bilinear")
	else:
		imgResized = resize_skimage(img, width=width, height=height, interpolation="bilinear")
	return imgResized

def doResizePng(img, inPath, height, width):
	if "semantic" in inPath:
		imgResized = resize_opencv(img, width=width, height=height, interpolation="nearest")
	elif ("depth" in inPath) or ("flow" in inPath):
		decodedImg = unrealFloatFromPng(img)
		imgResized = resize_skimage(decodedImg, width=width, height=height, interpolation="bilinear")
		imgResized = unrealPngFromFloat(imgResized)
	else:
		imgResized = resize_skimage(img, width=width, height=height, interpolation="bilinear")
	return imgResized

def doBatch(args, thisFiles):
	inPaths = list(map(lambda x : "%s/%s" % (args.baseDirectory, x), thisFiles))
	if args.mode == "images":
		outPaths = list(map(lambda x : "%s/%s" % (args.resultDirectory, x), thisFiles))
		doResizeFn = doResizePng
		saveFn = lambda path, file : tryWriteImage(file, path, imgLib="lycon")
	elif args.mode == "npy":
		outPaths = list(map(lambda x : "%s/%s" % (args.resultDirectory, x.replace("png", "npy")), thisFiles))
		doResizeFn = doResizeNpy
		saveFn = np.save

	imgs = [tryReadImage(inPath) for inPath in inPaths]

	results = []
	for img, path in zip(imgs, inPaths):
		results.append(doResizeFn(img, path, height=args.desiredShape[0], width=args.desiredShape[1]))

	for result, path in zip(results, outPaths):
		saveFn(path, result)

def main():
	args = getArgs()
	files = list(filter(lambda x : x.endswith(".png"), map(lambda x : x.name, os.scandir(args.baseDirectory))))
	# # # WireframeThicker
	# files = list(filter(lambda x : (x.find("Thicker") == -1), files))
	# # # rgbDomain2
	# files = list(filter(lambda x : (x.find("Domain") == -1), files))
	# # # Optical Flow
	# files = list(filter(lambda x : (x.find("flow") == -1), files))

	# Just flow
	# files = list(filter(lambda x : (x.find("flowx1") != -1) or (x.find("flowy1") != -1), files))

	# files = list(filter(lambda x : (x.find("wireframe") != -1) and (x.find("Thicker") == -1), files))
	uniques = set(map(lambda x : x.split("_")[1], files))
	print("Found %d files to be converted. Unique representations: %s" % (len(files), uniques))
	Path(args.resultDirectory).mkdir(parents=True, exist_ok=True)

	# files = sorted(files)
	# B = list(filter(lambda x : x.find("_rgb_")!=-1, files))[0:100]
	# from copy import copy
	# newFiles = copy(B)
	# for unique in uniques:
	# 	if unique != "rgb":
	# 		C = list(map(lambda x : x.replace("rgb", unique), B))
	# 		newFiles.extend(C)
	# files = newFiles

	N = len(files) // args.batchSize + (len(files) % args.batchSize != 0)
	for i in tqdm(range(N)):
		startIndex, endIndex = i * args.batchSize, min((i + 1) * args.batchSize, len(files))
		thisFiles = files[startIndex : endIndex]

		doBatch(args, thisFiles)

if __name__ == "__main__":
	main()
