import os
import h5py
from pathlib import Path
from argparse import ArgumentParser
from tqdm import trange

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("semisupervisedSetH5Path")
    parser.add_argument("pseudolabelsDirPath")
    parser.add_argument("key")
    args = parser.parse_args()
    return args

def main():
    args = getArgs()

    file = h5py.File(args.semisupervisedSetH5Path, "r")
    baseDirectory = file["others"]["baseDirectory"][()]

    # Make sure we are using a semisupervised set
    assert len(file["train"]["rgb"]) == len(file["test"]["rgb"])
    assert file["train"]["rgb"][0] == file["test"]["rgb"][0]
    # Make sure we have the key available
    assert args.key in file["train"], "Key '%s' not found in %s. Existing keys: %s" % \
        (args.key, args.semisupervisedSetH5Path, list(file["train"]))

    dummyPaths = list(map(lambda x : "%s/%s" % (baseDirectory, str(x, "utf8")), file["train"][args.key][:]))
    assert len(dummyPaths) > 0
    for dummyPath in dummyPaths:
        assert Path(dummyPath).exists(), "Does not exist: %s" % dummyPath
    print("Found %d npy files to be replaced for key '%s'" % (len(dummyPaths), args.key))

    pseudolabelsPath = list(Path(args.pseudolabelsDirPath).glob("*.npy"))
    assert len(pseudolabelsPath) == len(dummyPaths), "%d vs %d" % (len(pseudolabelsPath), len(dummyPaths))
    # The assumption is that pseudolabels are called 0.npy, ..., N.npy
    for i in range(len(dummyPaths)):
        assert Path("%s/%d.npy" % (args.pseudolabelsDirPath, i)).exists()
    print("Found corresponding npy files in %s" % args.pseudolabelsDirPath)

    val = input("Press any key to continue. Ctrl+C to abandon.")
    for i in trange(len(dummyPaths), desc="Pseudolabels %s" % args.key):
        source = "%s/%d.npy" % (args.pseudolabelsDirPath, i)
        destination = dummyPaths[i]
        os.remove(destination)
        os.symlink(source, destination)

    print("Done!")

if __name__ == "__main__":
    main()