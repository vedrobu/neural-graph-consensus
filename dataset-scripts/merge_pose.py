import h5py
import numpy as np
from pathlib import Path
from tqdm import tqdm
from argparse import ArgumentParser
from neural_wrappers.utilities import deepCheckEqual

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("semiSupervisedH5Path")
    parser.add_argument("pseudoLabelsDirPath")
    args = parser.parse_args()
    return args

def main():
    args = getArgs()
    file = h5py.File(args.semiSupervisedH5Path, "a")
    posePseudoLabels = [str(f) for f in Path(args.pseudoLabelsDirPath).glob("*.npy")]
    positions = file["train"]["position"][()]
    assert deepCheckEqual(positions, file["validation"]["position"][()])
    assert len(positions) == len(posePseudoLabels)
    for i in range(len(positions)):
        p = Path("%s/%d.npy" % (args.pseudoLabelsDirPath, i))
        assert p.exists() and p.is_file()
    if not "position_gt" in file["train"]:
        file["train"]["position_gt"] = positions
    else:
        print("There is already the GT of pose in this dataset. This step would overwrite it with pseudolabels")
    for i in tqdm(range(len(positions))):
        file["train"]["position"][i] = np.load("%s/%d.npy" % (args.pseudoLabelsDirPath, i))

if __name__ == "__main__":
    main()
