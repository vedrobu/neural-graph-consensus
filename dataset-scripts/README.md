## Dataset information

## Exporting pseudolabels

This module holds the scripts to generate the original train_set as well as semisupervised train sets and iteration 1, 2,... train_sets based on pseudolabels.

# Steps

**0. Prerequisites**

  An existing dataset with RGB and at least one more representation. We'll call it train_set_256.h5, exported with carla_h5_paths_exporter & dataset_resize, for example:

  `python dataset_resize.py /path/to/train_set_images train_set_256_npy_dir npy --desiredShape=256,256`

  __Note:__ This script makes some assumptions about how the raw data looks like (expecting rgb images to contain rgb in the name, wireframe to contain wireframe etc. Update it accordingly to your dataset).

  `python ~/libs/neural-wrappers/reader_converters/carla_h5_converter.py train_set_256_npy_dir/ train_set.h5 paths --export_type=regular --split_keys=train,validation --splits=80,20`

  __Note:__ This is the only time we create a validation set. All subsequent steps will copy this validation, while updating the train set with semiseupervised data.

**1. New simulation raw RGB data:**

Make a new simulation (flight, drive, carla export, bike ride etc.) to get new raw RGB data. Put it into a dir called semisupervised_set_1.

**2. Create NPY files, but just for RGB data**

  `python dataset_resize.py /path/to/semisupervised_set_1 semisupervised_set_1_256_npy_dir npy --desiredShape=256,256`

**3. Create dummy NPY files for the other representations**

Our semisupervised set now has only RGB data converted to NPY. However, our train set has multiple representations (depth, wireframe etc.). We need to create dummy NPY files which will be updated with pseudolabels.

For example, here we'll replace \_rgb\_ name with \_cameranormal\_, ..., \_semanticSegmentation\_. Basically we copy the name of each RGB file and create a new file with the replaced string. This may vary from dataset to dataset.

`python generate_dummy_npy.py semisupervised_set_1_256_npy_dir/ --new_keys=_cameranormal_,_depth_,_halftone_,_normal_,_wireframe_,_semanticSegmentation_`

After this step we should have inside `semisupervised_set_1_256_npy_dir` `N` RGB npy files and `N * keys` dummy NPY files (0 bytes).

**4. Create the h5 file for the semisupervised set**

  `python ~/libs/neural-wrappers/reader_converters/carla_h5_converter.py semisupervised_set_1_256_npy_dir semisupervised_set_1.h5 paths --export_type=test --statistics_file=train_set.h5`

__Note:__ We reuse train set's statistics here.

__Note:__ Export type test means no validation set is computed (100% of data is put into 'train' key inside the h5 file)

**5. Merge the two h5 sets**

`python merge_iteration_datasets.py train_set.h5 semisupervised_set_1.h5 train_set_iteration1.h5`

After this step we have a new h5 file that is the combination of the two sets. However, we don't have any data for the other representations of the semisupervised set (they are paths to dummy npy files).

**6. Export pseudolabels**

__Note:__ Here we'll use an example of exporting `depth` pseudolabels.

`python ../single_links_train/main.py export_pseudolabels_npy rgb2depth_graph_ensemble_best semisupervised_set_1.h5 --resolution=256,256 --singleLinksPath=/path/to/rgb2depth_graph_ensemble_single_links --dir=pseudolabels_depth_dir`

After this step, in the directory `pseudolabels_depth_dir` we are left with N depth predictions (called 0.npy, ..., N.npy). These must replace the dummy files in the correct order inside the H5 File.

Now, we must update the dummy NPY files with these predictions (assumption is that 0.npy maps to the dummy file at index 0 in semisupervised_set, 1.npy to [1], ..., N.npy to [N])

`python update_pseudolabels.py semisupervised_set_1.h5 pseudolabels_depth_dir depth`

Since both `train_set_iteration1.h5` and `semisupervised_set_1.h5` use the same paths and since we just create symbolic links, we are ready to train on the new data.

**7. Train the single link with new data**

`python ../single_links_train/main.py train rgb2depth train_set_iteration1.h5 --resolution=256,256 --dir=rgb2depth_dsl1`
