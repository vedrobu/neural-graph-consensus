import numpy as np
import torch as tr
import h5py
import torch.optim as optim
from argparse import ArgumentParser
from neural_wrappers.utilities import changeDirectory, getGenerators
from neural_wrappers.callbacks import SaveModels, SaveHistory, PlotMetrics, Callback
from neural_wrappers.schedulers import ReduceLRAndBacktrackOnPlateau
from neural_wrappers.readers import CarlaH5PathsReader, StaticBatchedDatasetReader

from cycleconcepts.utils import getModelDataDimsAndHyperParams, fullPath, addNodesArguments
# from cycleconcepts.reader import Reader
from cycleconcepts.nodes import *
from cycleconcepts.models import *
from cycleconcepts.cfgs import *

from test_export_images import test_export_images
from export_pseudolabels import export_pseudolabels
from test_export_video import test_export_video
from test_dataset import test_dataset

tr.backends.cudnn.deterministic = True
tr.backends.cudnn.benchmark = False

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("graph_config")
	parser.add_argument("dataset_path")

	# Models
	parser.add_argument("--weightsFile")
	parser.add_argument("--firstLinksPath", help="Used by two-hops links using pre-trained first hop.")
	parser.add_argument("--singleLinksPath", help="Used by two-hops links using pre-trained first hop.")

	# Training stuff
	parser.add_argument("--batch_size", default=5, type=int)
	parser.add_argument("--num_epochs", default=100, type=int)
	parser.add_argument("--lr", default=0.001, type=float)
	parser.add_argument("--seed", default=0, type=int)
	parser.add_argument("--patience", default=0, type=int)
	parser.add_argument("--factor", type=float)
	parser.add_argument("--optimizer", default="adamw")

	# Reader stuff
	parser.add_argument("--resolution", default="512,512")

	# Various nodes hyperparameters
	parser = addNodesArguments(parser)

	parser.add_argument("--dir")
	parser.add_argument("--show", default=1, type=int)

	args = parser.parse_args()
	assert args.type in ("train", "test", "test_export_images", "retrain", "test_dataset", "export_pseudolabels")

	args.resolution = tuple(map(lambda x : int(x), args.resolution.split(",")))
	args.weightsFile = fullPath(args.weightsFile)
	args.dir = fullPath(args.dir)
	args.dataset_path = fullPath(args.dataset_path)
	args.firstLinksPath = fullPath(args.firstLinksPath)
	args.singleLinksPath = fullPath(args.singleLinksPath)

	# Water the plants
	np.random.seed(args.seed)
	tr.manual_seed(args.seed)
	return args

def main():
	args = getArgs()
	model, dataDims, hyperParameters = getModelDataDimsAndHyperParams(args, globals()[args.graph_config])
	numNeighboursAhead = hyperParameters["opticalFlowSkip"] if "opticalFlowSkip" in hyperParameters else 0
	reader = StaticBatchedDatasetReader(
		CarlaH5PathsReader(h5py.File(args.dataset_path, "r")["train"], dataBuckets={"data" : dataDims}, 
			desiredShape=hyperParameters["resolution"], numNeighboursAhead=numNeighboursAhead,
			hyperParameters=hyperParameters),
		args.batch_size)
	validationReader = StaticBatchedDatasetReader(
		CarlaH5PathsReader(h5py.File(args.dataset_path, "r")["validation"], dataBuckets={"data" : dataDims}, 
			desiredShape=hyperParameters["resolution"], numNeighboursAhead=numNeighboursAhead,
			hyperParameters=hyperParameters),
		args.batch_size)
	print(reader)
	print(validationReader)
	generator, valGenerator = reader.iterate(), validationReader.iterate()

	if args.type == "test_dataset":
		changeDirectory(args.dir, expectExist=False)
		test_dataset(generator)
		exit()

	optimizerType = {
		"rmsprop" : optim.RMSprop,
		"adamw" : optim.AdamW
	}[args.optimizer]
	hyperParameters["optimizerType"] = args.optimizer
	model.addHyperParameters(hyperParameters)
	model.setOptimizer(optimizerType, lr=args.lr)
	if args.patience > 0:
		model.setOptimizerScheduler(ReduceLRAndBacktrackOnPlateau(model, "Loss", args.patience, args.factor))
	edgeMetricNames = ["Loss"]
	# for edge in model.edges:
	# 	# breakpoint()
	# 	for callback in edge.getMetrics():
	# 		breakpoint()
	# 		edgeMetricNames.append((str(edge), name))
	# 		edgeDirections.append(callback.getDirection())
	# saveModels = [SaveModels("best", name) for (name, direction) in zip(edgeMetricNames, edgeDirections)]
	saveModels = [SaveModels("best", name) for name in edgeMetricNames]
	saveModels.append(SaveModels("last", "Loss"))
	model.addCallbacks([SaveHistory("history.txt"), *saveModels, PlotMetrics(edgeMetricNames)])
	print(model.summary())

	if args.type == "train":
		changeDirectory(args.dir, expectExist=False)
		model.draw("graph")
		model.train_generator(generator, len(generator), args.num_epochs, valGenerator, len(valGenerator))
	elif args.type == "retrain":
		model.loadModel(args.weightsFile)
		changeDirectory(args.dir, expectExist=True)
		model.train_generator(generator, len(generator), args.num_epochs, valGenerator, len(valGenerator))
	elif args.type == "test":
		if not args.weightsFile is None:
			model.loadWeights(args.weightsFile)
		# sys.path.append("../other-scripts")
		# from median_callback import MedianCallback
		# model.addCallbacks([MedianCallback(args.graph_config, reader)])
		# model.addCallbacks([MyCallback(args.graph_config, reader)])
		results = model.test_generator(valGenerator, len(valGenerator), printMessage="v2")
		print("\nResults: %s" % str(results))
	elif args.type == "test_export_images":
		if not args.weightsFile is None:
			model.loadWeights(args.weightsFile)
		model.eval()
		changeDirectory(args.dir, expectExist=False)
		test_export_images(model, generator, numStlen(generator), valGenerator, len(valGenerator), args.show)
	elif args.type == "export_pseudolabels":
		if not args.weightsFile is None:
			model.loadWeights(args.weightsFile)
		changeDirectory(args.dir, expectExist=False)
		export_pseudolabels(model, valGenerator, len(valGenerator))

if __name__ == "__main__":
	main()
