## Iteration 0

export X=rgb2depth; CUDA_VISIBLE_DEVICES=7 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration0/$X/model_best_Loss.pkl
export X=rgb2wireframeregression; CUDA_VISIBLE_DEVICES=6 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration0/rgb2wireframe/model_best_Loss.pkl
export X=rgb2normal; CUDA_VISIBLE_DEVICES=5 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration0/$X/model_best_Loss.pkl
export X=rgb2cameranormal; CUDA_VISIBLE_DEVICES=4 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration0/$X/model_best_Loss.pkl
export X=rgb2semantic; CUDA_VISIBLE_DEVICES=3 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration0/$X/model_best_Loss.pkl

## G-E #1

export X=rgb2depth_graph_ensemble; CUDA_VISIBLE_DEVICES=7 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration0
export X=rgb2wireframeregression_graph_ensemble; CUDA_VISIBLE_DEVICES=6 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration0
export X=rgb2normal_graph_ensemble; CUDA_VISIBLE_DEVICES=5 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration0
export X=rgb2cameranormal_graph_ensemble; CUDA_VISIBLE_DEVICES=4 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration0
export X=rgb2semantic_graph_ensemble; CUDA_VISIBLE_DEVICES=3 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration0

## D-SL #1

export X=rgb2depth; CUDA_VISIBLE_DEVICES=7 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration1/$X/model_best_Loss.pkl
export X=rgb2wireframeregression; CUDA_VISIBLE_DEVICES=6 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration1/rgb2wireframe/model_best_Loss.pkl
export X=rgb2normal; CUDA_VISIBLE_DEVICES=5 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration1/$X/model_best_Loss.pkl
export X=rgb2cameranormal; CUDA_VISIBLE_DEVICES=4 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration1/$X/model_best_Loss.pkl
export X=rgb2semantic; CUDA_VISIBLE_DEVICES=3 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration1/$X/model_best_Loss.pkl

## G-E #2

export X=rgb2depth_graph_ensemble; CUDA_VISIBLE_DEVICES=7 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration1
export X=rgb2wireframeregression_graph_ensemble; CUDA_VISIBLE_DEVICES=6 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration1
export X=rgb2normal_graph_ensemble; CUDA_VISIBLE_DEVICES=5 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration1
export X=rgb2cameranormal_graph_ensemble; CUDA_VISIBLE_DEVICES=4 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration1
export X=rgb2semantic_graph_ensemble; CUDA_VISIBLE_DEVICES=3 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration1

## D-SL #2

export X=rgb2depth; CUDA_VISIBLE_DEVICES=7 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration2/$X/model_best_Loss.pkl
export X=rgb2wireframeregression; CUDA_VISIBLE_DEVICES=6 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration2/rgb2wireframe/model_best_Loss.pkl
export X=rgb2normal; CUDA_VISIBLE_DEVICES=5 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration2/$X/model_best_Loss.pkl
export X=rgb2cameranormal; CUDA_VISIBLE_DEVICES=4 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration2/$X/model_best_Loss.pkl
export X=rgb2semantic; CUDA_VISIBLE_DEVICES=3 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --weightsFile=../results/paper_results/iteration2/$X/model_best_Loss.pkl

## G-E #3

export X=rgb2depth_graph_ensemble; CUDA_VISIBLE_DEVICES=7 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration2
export X=rgb2wireframeregression_graph_ensemble; CUDA_VISIBLE_DEVICES=6 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration2
export X=rgb2normal_graph_ensemble; CUDA_VISIBLE_DEVICES=5 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration2
export X=rgb2cameranormal_graph_ensemble; CUDA_VISIBLE_DEVICES=4 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration2
export X=rgb2semantic_graph_ensemble; CUDA_VISIBLE_DEVICES=3 python main.py test $X /scratch/nvme0n1/Datasets/Carla/final_dataset/test_set_256.h5 --resolution=256,256 --semanticNumClasses=13 --maxDepthMeters=300 --batch_size=20 --singleLinksPath=../results/paper_results/iteration2

