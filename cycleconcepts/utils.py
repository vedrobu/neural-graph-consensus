import os
import h5py
import numpy as np
from neural_wrappers.pytorch import device
from neural_wrappers.graph_stable import Node
from typing import get_type_hints
from functools import partial
from .nodes import *
from .models import *

def getModelDataDimsAndHyperParams(args, modelType):
	def getPositionExtremes(datasetPath):
		file = h5py.File(datasetPath, "r")
		stats = file["others"]["dataStatistics"]
		return {k : stats["position"][k][0 : 3] for k in ["min", "max"]}

	def depthHypers(args):
		assert not args.maxDepthMeters is None
		return {"maxDepthMeters" : args.maxDepthMeters}

	def poseHypers(args):
		assert not args.positionWeights is None
		assert not args.orientationWeights is None
		args.positionWeights = np.array(list(map(lambda x : float(x), \
			args.positionWeights.split(","))), dtype=np.float32)
		args.orientationWeights = np.array(list(map(lambda x : float(x), \
			args.orientationWeights.split(","))), dtype=np.float32)
		positionsExtremes = getPositionExtremes(args.dataset_path)

		return {
			"positionLossType" : args.positionLossType,
			"positionWeights" : args.positionWeights,
			"positionNormalization" : args.positionNormalization,
			"positionsExtremes" : positionsExtremes,
			"orientationLossType" : args.orientationLossType,
			"orientationWeights" : args.orientationWeights,
			"orientationNormalization" : args.orientationNormalization,
			"orientationRepresentation" :  args.orientationRepresentation
		}

	def semanticHypers(args):
		assert not args.semanticNumClasses is None
		return {"semanticNumClasses" : args.semanticNumClasses}

	hyperParameters = {
		RGB : lambda : {},
		Depth : lambda : depthHypers(args),
		Pose : lambda : poseHypers(args),
		Semantic : lambda : semanticHypers(args)
	}

	reverseHyperParameters = {
		"maxDepthMeters" : Depth,
		"positionLossType" : Pose,
		"positionWeights" : Pose,
		"positionNormalization" : Pose,
		"positionsExtremes" : Pose,
		"orientationLossType" : Pose,
		"orientationWeights" : Pose,
		"orientationNormalization" : Pose,
		"orientationRepresentation" :  Pose,
		"semanticNumClasses" : Semantic,
		"firstLinksPath" : "firstLinks",
		"singleLinksPath" : "singleLinks"
	}

	# Fake all hyperparametrs so we can initialize the model.
	hypers = {"resolution" : args.resolution}

	if get_type_hints(modelType)["return"] == GraphEnsemble:
		assert args.weightsFile is None
		assert not args.singleLinksPath is None
		modelType = partial(modelType, singleLinksPath=args.singleLinksPath)
	elif get_type_hints(modelType)["return"] == TwoHopsGraph:
		if args.type != "retrain":
			assert args.weightsFile is None
		assert not args.firstLinksPath is None
		modelType = partial(modelType, firstLinksPath=args.firstLinksPath)
	elif get_type_hints(modelType)["return"] == SingleLinkGraph:
		if args.type in ("test", "export_pseudolabels"):
			assert not args.weightsFile is None

	while True:
		try:
			Node.lastNodeID = 0
			model = modelType(hyperParameters=hypers).to(device)
			break
		except Exception as e:
			if len(str(e).split(" ")) > 1:
				raise Exception(e)

			key = str(e)[1 : -1]
			node = reverseHyperParameters[key]
			thisHypers = hyperParameters[node]()
			for k in thisHypers:
				hypers[k] = thisHypers[k]

	dataDims = []
	if args.type == "export_pseudolabels":
		dataDims = ["rgb"]
	else:
		for node in model.nodes:
			GT = node.groundTruthKey
			if type(GT) == str:
				dataDims.append(GT)
			else:
				assert type(GT) == list
				dataDims.extend(GT)

	return model, dataDims, hypers

def fullPath(path):
	if not path:
		return None
	return os.path.abspath(path)

def addNodesArguments(parser):
	parser.add_argument("--opticalFlowPercentage", help="Hyperparameter for OpticalFlow in format X%,Y%")
	parser.add_argument("--opticalFlowMode", help="Valid: xy, magnitude, xy_plus_magnitude")
	parser.add_argument("--opticalFlowSkip", type=int)
	parser.add_argument("--maxDepthMeters", type=float)
	parser.add_argument("--dotRadius", type=int)
	parser.add_argument("--positionLossType")
	parser.add_argument("--positionWeights")
	parser.add_argument("--positionNormalization")
	parser.add_argument("--orientationLossType")
	parser.add_argument("--orientationWeights")
	parser.add_argument("--orientationNormalization")
	parser.add_argument("--orientationRepresentation")
	parser.add_argument("--semanticNumClasses", type=int)
	# parser.add_argument("--threshold", type=float)
	return parser

# Function that takes a prediction and converts it back to renormalized npy
# Usage: Can be used for pseuodolabels such that the reader normalizes it correctly
def makeNpy(blended, obj):
	Type = type(obj)
	strType = str(Type).split(".")[-1][0:-2]
	assert strType in ("Semantic", "CameraNormal", "Normal", "Wireframe", "Pose", "Depth", \
		"Halftone", "WireframeRegression")
	if strType == "Semantic":
		Keys = [(0, 0, 0), (70, 70, 70), (153, 153, 190), (160, 170, 250), (60, 20, 220), \
			(153, 153, 153), (50, 234, 157), (128, 64, 128), (232, 35, 244), (35, 142, 107), \
			(142, 0, 0), (156, 102, 102), (0, 220, 220)]
		argmaxBlended = np.argmax(blended, axis=-1)
		result = np.zeros((*argmaxBlended.shape, 3), dtype=np.uint8)
		for i in range(13):
			whereI = np.where(argmaxBlended == i)
			result[whereI] = Keys[i]
	elif strType in ("CameraNormal", "Normal", "Halftone", "WireframeRegression"):
		result = np.clip(blended, 0, 1) * 255
	elif strType == "Pose":
		def positionRenorm(x, positionsExtremes):
			Min, Max = positionsExtremes["min"], positionsExtremes["max"]
			y = np.clip(x[0 : 3], 0, 1)
			y = y * (Max - Min) + Min
			return y

		def orientationRenorm(x):
			# x :: [0 : 1] -> [-1 : 1]
			y = np.clip(x[3 :], 0, 1) * 2 - 1
			# y :: [-1 : 1] -> [-pi : pi]
			# y = np.array(txe.quat2euler(y, "sxyx"))
			# y :: [-pi : pi] -> [-1 : 1]
			# y /= np.pi
			# y :: [-1 : 1] -> [-180 : 180]
			y *= 180
			return y
		translation = positionRenorm(blended, obj.positionsExtremes)
		orientation = orientationRenorm(blended)
		result = np.concatenate([translation, orientation])
	elif strType == "Depth":
		# result :: [0 : 1] representing [0 : maxDepthMeters]. We need to renormalize it to [0 : 1000m]
		result = np.clip(blended, 0, 1)[..., 0]
		# result :: [0 : 1] => [0 : maxDepthMeters]
		result = result * obj.maxDepthMeters
		# result :: [0 : maxDepthMeters] => [0 : 1] where 1 represents 1000m
		result = result / 1000

	return result