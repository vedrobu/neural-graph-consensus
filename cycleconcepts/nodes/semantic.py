import numpy as np
import torch as tr
import torch.nn.functional as F
from functools import partial
from neural_wrappers.graph_stable.node import MapNode, VectorNode
from neural_wrappers.utilities import pickTypeFromMRO
from neural_wrappers.pytorch import trModuleWrapper, device, FeedForwardNetwork
from neural_wrappers.metrics import Accuracy, MetricWrapper, Metric, MeanIoU

from ..models import EncoderMap2Map, EncoderMap2Vector, DecoderMap2Map, DecoderVector2Map

class SoftmaxDecoder(FeedForwardNetwork):
	def __init__(self, baseDecoder):
		super().__init__()
		self.baseDecoder = baseDecoder

	def forward(self, x):
		yDecoder = self.baseDecoder(x)
		y = F.softmax(yDecoder, dim=-1)
		return y

class Semantic(MapNode):
	def __init__(self, semanticNumClasses):
		super().__init__(name="Semantic", groundTruthKey="semantic_segmentation")
		self.numClasses = semanticNumClasses

	def getEncoder(self, outputNodeType=None):
		modelTypes = {
			MapNode : partial(EncoderMap2Map, dIn=self.numClasses),
			VectorNode : partial(EncoderMap2Vector, dIn=self.numClasses),
			# type(None) : ModelNormal # TODO
		}
		model = pickTypeFromMRO(outputNodeType, modelTypes)()
		return model

	def getDecoder(self, inputNodeType=None):
		modelTypes = {
			MapNode : partial(DecoderMap2Map, dOut=self.numClasses),
			VectorNode : partial(DecoderVector2Map, dOut=self.numClasses)
		}
		model = pickTypeFromMRO(inputNodeType, modelTypes)()
		# Semantic models require to have a softmax head, so we have a small wrapper that just does this on top of the
		#  generic model. This means that any specific model that is going to be used (or node that subtypes this node)
		#  must be aware and not call softmax at the end so it isn't called twice.
		return SoftmaxDecoder(model)

	def getMetrics(self):
		return {
			"Accuracy" : Accuracy(),
			"Mean IoU (global)" : MeanIoU(mode="global")
		}

	def getCriterion(self):
		return Semantic.lossFn

	@staticmethod
	def lossFn(y, t):
		t = t.type(tr.bool)
		L = y[t]
		L = -tr.log(L)
		L = L.mean()
		return L