# Custom nodes go here
from .rgb import RGB
from .pose import Pose
from .depth import Depth
from .normal import Normal
from .cameranormal import CameraNormal
from .semantic import Semantic
from .halftone import Halftone
from .wireframeregression import WireframeRegression