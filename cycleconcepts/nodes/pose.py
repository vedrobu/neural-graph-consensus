import numpy as np
import transforms3d.euler as txe
import torch as tr
from functools import partial
from overrides import overrides
from neural_wrappers.graph_stable.node import MapNode, VectorNode
from neural_wrappers.utilities import pickTypeFromMRO, NWNumber
from neural_wrappers.pytorch import trGetData
from neural_wrappers.metrics import Metric

from ..models import EncoderVector2Map, DecoderMap2Vector

def isPowerOfTwo(x):
	return np.log2(x) == int(np.log2(x))

class PositionMetric(Metric):
	def __init__(self, hyperParameters):
		super().__init__()
		self.positionsExtremes = hyperParameters["positionsExtremes"]

	@overrides
	def __call__(self, results : NWNumber, labels : NWNumber, **kwargs):
		results = results[:, 0 : 3]
		labels = labels[:, 0 : 3]
		error = np.linalg.norm(results - labels, axis=-1)
		return error.mean()

class OrientationMetric(Metric):
	def __init__(self, hyperParameters):
		super().__init__()

	@overrides
	def epochReduceFunction(self, results) -> NWNumber:
		return results.mean()

	@overrides
	def __call__(self, results : NWNumber, labels : NWNumber, **kwargs):
		results = results[:, 3 : ]
		labels = labels[:, 3 : ]
		ySin, tSin = results[:, 0 : 3], labels[:, 0 : 3]
		yCos, tCos = results[:, 3 :], labels[:, 3 :]
		yNorm = (ySin**2 + yCos**2)
		yOr = np.arctan2(ySin / yNorm, yCos / yNorm)
		tOr = np.arctan2(tSin, tCos)

		# yOr and tOr are in [-pi : pi] => [-1 : 1]
		yOr /= np.pi
		tOr /= np.pi

		# https://stackoverflow.com/questions/1878907/the-smallest-difference-between-2-angles
		# [-1 : 1] => [0 : 1]
		error = np.abs((tOr - yOr + 1) % 2 - 1)

		# error :: [0 : 1] => [0 : 180]
		error = error * 180
		return error.mean(axis=0)

class Pose(VectorNode):
	def __init__(self, **hyperParameters):
		# TODO outResolution is just for decoder.
		outResolution = hyperParameters["resolution"]
		assert hyperParameters["poseUsePosition"] + hyperParameters["poseUseOrientation"] > 0
		assert isPowerOfTwo(outResolution[0]) and outResolution[0] == outResolution[1] and outResolution[0] >= 64

		relevantHyperParams = {}
		self.numDimensions = 0
		# Position hyperparameters
		if hyperParameters["poseUsePosition"] == True:
			assert "positionLossType" in hyperParameters
			assert "positionWeights" in hyperParameters
			assert "positionsExtremes" in hyperParameters
			assert "positionNormalization" in hyperParameters
			assert hyperParameters["positionLossType"] in ("L1", "L2")
			assert hyperParameters["positionNormalization"] in ("min_max", "none")
			assert len(hyperParameters["positionWeights"]) == 3
			relevantHyperParams["positionLossType"] = hyperParameters["positionLossType"]
			relevantHyperParams["positionWeights"] = hyperParameters["positionWeights"]
			relevantHyperParams["positionsExtremes"] = hyperParameters["positionsExtremes"]
			relevantHyperParams["positionNormalization"] = hyperParameters["positionNormalization"]
			self.numDimensions += 3

		if hyperParameters["poseUseOrientation"] == True:
			assert "orientationLossType" in hyperParameters
			assert "orientationWeights" in hyperParameters
			assert "orientationNormalization" in hyperParameters
			assert "orientationRepresentation" in hyperParameters
			assert hyperParameters["orientationLossType"] in ("L1", "L2")
			assert hyperParameters["orientationNormalization"] in ("sin_cos", )
			assert hyperParameters["orientationRepresentation"] in ("euler", )
			assert len(hyperParameters["orientationWeights"]) == 3
			relevantHyperParams["orientationLossType"] = hyperParameters["orientationLossType"]
			relevantHyperParams["orientationWeights"] = hyperParameters["orientationWeights"]
			relevantHyperParams["orientationNormalization"] = hyperParameters["orientationNormalization"]
			relevantHyperParams["orientationRepresentation"] = hyperParameters["orientationRepresentation"]
			self.numDimensions += 3


		# if orientationRepresentation == "euler":
		# 	assert orientationNormalization in ("min_max", "none", "sin_cos")
		# 	if orientationNormalization in ("min_max", "none"):
		# 		self.numDimensionsOrientation = 3
		# 	else:
		# 		self.numDimensionsOrientation = 6
		# elif poseRepresentation == "quaternion":
		# 	assert orientationNormalization in ("min_max", "none")
		# 	self.numDimensionsOrientation = 4
		# assert len(orientationWeights) == self.numDimensionsOrientation
		self.positionMetric = PositionMetric(relevantHyperParams)
		self.orientationMetric = OrientationMetric(relevantHyperParams)

		super().__init__(name="Pose", groundTruthKey="pose", hyperParameters=relevantHyperParams)

	def getEncoder(self, outputNodeType=None):
		numConvTransposed = int(np.log2(self.outResolution[0])) - 3
		modelTypes = {
			MapNode : partial(EncoderVector2Map, dIn=self.numDimensions, numConvTransposed=numConvTransposed)
		}
		return pickTypeFromMRO(outputNodeType, modelTypes)()

	def getDecoder(self, inputNodeType=None):
		modelTypes = {
			MapNode : partial(DecoderMap2Vector, dOut=self.numDimensions)
		}
		return pickTypeFromMRO(inputNodeType, modelTypes)()

	def getMetrics(self):
		assert self.hyperParameters["orientationNormalization"] in ("sin_cos")
		assert self.hyperParameters["orientationRepresentation"] in ("euler", "quaternion")
		metrics = {}
		if hyperParameters["poseUsePosition"] == True:
			metrics["Position (m)"] = self.positionMetric
		if hyperParameters["poseUseOrientation"] == True:
			metrics["Orientation (deg)"] = self.orientationMetric
		return metrics

	def getPositionCriterion(self):
		trPositionWeights = trGetData(self.hyperParameters["positionWeights"])
		positionLossFn = {
			"L1" : Pose.lossFnL1,
			"L2" : Pose.lossFnL2
		}[self.hyperParameters["positionLossType"]]
		return partial(positionLossFn, weights=trPositionWeights)

	def getCriterion(self):
		return lambda y, t : Pose.poseLoss(y, t, self)

	def poseLoss(y, t, obj):
		trPositionWeights = trGetData(obj.hyperParameters["positionWeights"])
		trOrientationWeights = trGetData(obj.hyperParameters["orientationWeights"])

		positionLossFn = {
			"L1" : Pose.lossFnL1,
			"L2" : Pose.lossFnL2
		}[obj.hyperParameters["positionLossType"]]

		orientationLossFn = {
			"L1" : Pose.lossFnL1,
			"L2" : Pose.lossFnL2
		}[obj.hyperParameters["orientationLossType"]]

		yPos, tPos = y[:, 0 : 3], t[:, 0 : 3]
		LPos = positionLossFn(yPos, tPos, trPositionWeights)

		ySin, tSin = y[:, 3 : 6], t[:, 3 : 6]
		yCos, tCos = y[:, 6 :], t[:, 6 :]
		yNorm = (ySin**2 + yCos**2)
		yOr = tr.atan2(ySin / yNorm, yCos / yNorm)
		tOr = tr.atan2(tSin, tCos)
		LOr = orientationLossFn(yOr, tOr, trOrientationWeights)

		LSinCos = yNorm.abs().mean()
		return LPos + LOr + LSinCos

	def lossFnL1(y, t, weights):
		L = (y - t).abs() * weights
		return L.mean()

	def lossFnL2(y, t, weights):
		L = (y - t)**2 * weights
		return L.mean()
