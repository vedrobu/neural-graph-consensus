import numpy as np
from neural_wrappers.graph_stable.node import MapNode, VectorNode
from neural_wrappers.utilities import pickTypeFromMRO
from functools import partial
from ..models import EncoderMap2Map, EncoderMap2Vector, DecoderMap2Map, DecoderVector2Map

class Depth(MapNode):
	def __init__(self, maxDepthMeters):
		self.maxDepthMeters = maxDepthMeters
		hyperParameters = {"maxDepthMeters" : maxDepthMeters}
		super().__init__(name="Depth", groundTruthKey="depth", hyperParameters=hyperParameters)

	def getEncoder(self, outputNodeType=None):
		modelTypes = {
			MapNode : partial(EncoderMap2Map, dIn=1),
			VectorNode : partial(EncoderMap2Vector, dIn=1),
			# type(None) : ModelDepth # TODO
		}
		return pickTypeFromMRO(outputNodeType, modelTypes)()

	def getDecoder(self, inputNodeType=None):
		modelTypes = {
			MapNode : partial(DecoderMap2Map, dOut=1),
			VectorNode : partial(DecoderVector2Map, dOut=1)
		}
		return pickTypeFromMRO(inputNodeType, modelTypes)()

	def getMetrics(self):
		metrics = {
			"Depth (m)" : partial(Depth.depthMetric, maxDepthMeters=self.maxDepthMeters),
			"RMSE" : partial(Depth.rmseMetric, maxDepthMeters=self.maxDepthMeters)
		}
		return metrics

	def getCriterion(self):
		return Depth.lossFn

	def lossFn(y, t):
		L = ((y - t)**2).mean()
		return L

	def depthMetric(y, t, maxDepthMeters, **k):
		# Normalize back to meters, output is in [0 : 1] representing [0 : maxDepthMeters]m
		yDepthMeters = y * maxDepthMeters
		tDepthMeters = t * maxDepthMeters
		res = np.abs(yDepthMeters - tDepthMeters).mean()
		return res

	def rmseMetric(y, t, maxDepthMeters, **k):
		# Normalize back to milimeters, output is in [0 : 1] representing [0 : maxDepthMeters]m
		yDepthMeters = y * maxDepthMeters
		tDepthMeters = t * maxDepthMeters
		res = np.sqrt(((yDepthMeters - tDepthMeters) ** 2).mean())
		return res