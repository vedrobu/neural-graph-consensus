import numpy as np
import torch as tr
from functools import partial
from ..models import EncoderMap2Map, EncoderMap2Vector, DecoderMap2Map, DecoderVector2Map
from .rgb import RGB, MapNode
from neural_wrappers.utilities import pickTypeFromMRO, toCategorical
from neural_wrappers.pytorch import trModuleWrapper, device
from neural_wrappers.metrics import MeanIoU, Accuracy, MetricWrapper

accuracy = Accuracy()

def fromRegressionToMultiClass(results, labels, **kwargs):
	global accuracy
	# 3 (hopefully) identical channels => 1 channel
	results = results[..., 0 : 1]
	labels = labels[..., 0 : 1]

	# Since we have a [0 : 1] prediction, we'll put a threshold at 0.1 (10%) abs
	tBinary = labels > 0
	resultsCloseEnough = np.abs(results - labels) < 0.1
	yBinary = resultsCloseEnough == tBinary

	# Convert to two classes for accuracy purposes
	y = np.concatenate([1 - yBinary, yBinary], axis=-1).reshape(-1, 2)
	t = toCategorical(tBinary.astype(np.uint8), numClasses=2).astype(np.float32)

	return accuracy(y, t, **kwargs)

class WireframeRegression(RGB):
	def __init__(self):
		MapNode.__init__(self, name="WireframeRegression", groundTruthKey="wireframe_regression")

	def getMetrics(self):
		return {
			"Wireframe (L1 pixel)" : RGB.RGBMetricL1Pixel,
			"Wireframe (L2)" : RGB.RGBMetricL2,
			"Accuracy" : fromRegressionToMultiClass
		}