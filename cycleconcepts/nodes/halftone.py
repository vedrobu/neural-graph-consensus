from .rgb import RGB, MapNode

class Halftone(RGB):
	def __init__(self):
		MapNode.__init__(self, name="Halftone", groundTruthKey="halftone")

	def getMetrics(self):
		return {
			"Halftone (L1 pixel)" : Halftone.RGBMetricL1Pixel,
			"Halftone (L2)" : Halftone.RGBMetricL2
		}