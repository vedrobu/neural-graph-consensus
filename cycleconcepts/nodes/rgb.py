from neural_wrappers.graph_stable.node import MapNode, VectorNode
from neural_wrappers.utilities import pickTypeFromMRO
import numpy as np
from functools import partial
from ..models import EncoderMap2Map, EncoderMap2Vector, DecoderMap2Map, DecoderVector2Map

class RGB(MapNode):
	def __init__(self):
		super().__init__(name="RGB", groundTruthKey="rgb")

	def getEncoder(self, outputNodeType=None):
		modelTypes = {
			MapNode : partial(EncoderMap2Map, dIn=3),
			VectorNode : partial(EncoderMap2Vector, dIn=3),
			# type(None) : ModelRGB # TODO
		}
		return pickTypeFromMRO(outputNodeType, modelTypes)()

	def getDecoder(self, inputNodeType=None):
		modelTypes = {
			MapNode : partial(DecoderMap2Map, dOut=3),
			VectorNode : partial(DecoderVector2Map, dOut=3)
		}
		return pickTypeFromMRO(inputNodeType, modelTypes)()

	def getMetrics(self):
		return {
			"RGB (L1 pixel)" : RGB.RGBMetricL1Pixel,
			"RGB (L2)" : RGB.RGBMetricL2
		}

	def getCriterion(self):
		return RGB.lossFn

	def lossFn(y, t):
		return ((y - t)**2).mean()

	def RGBMetricL1Pixel(y, t, **k):
		# Remap y and t from [0 : 1] to [0 : 255]
		yRgbOrig = y * 255
		tRgbOrig = t * 255
		return np.abs(yRgbOrig - tRgbOrig).mean()

	def RGBMetricL2(y, t, **k):
		return ((y - t)**2).mean()
