# CycleConcepts

This is a subrepository containing the implementation of the nodes and models used and can be seen as the backbone of the project. The nodes are implemented on top of the `graph` implemented in the `neural-wrappers` library, which is a generic version with semantics defined: https://www.overleaf.com/read/vfbqdgxtxnws. Each node is either a MapNode (2D) or a VectorNode (1D). At the moment of writing this document, only these two are possible in the graph library.

Since this is a Computer Vision project, and more specific implementations of nodes and models have to be made. For this purpose, the repository has two main submodules:
- nodes
- models

## 1. Nodes
Under nodes, we implement various nodes used throughout the experiments. Each such node is a concept used in a 3D scene (hence the name). The nodes that are currently implemented are:
- RGB
- Depth
- Semantic
- Camera Normal
- World Normal
- Pose
- Halftone
- Wireframe
- Optical Flow
- others - these may or may not disappear at some point

### 1.1 RGB

This node is implemented under nodes/rgb.py and is a conceptual representation of an RGB image (3 channels). It is a MapNode and has the following metrics: 
- `L1 pixel` - The L1 error represented in pixels (0 - 255) averaged over the 3 channels
- `L2`

### 1.2 Depth

This node is implemented under nodes/depth.py and is a conceptual representation of an Depth image (1 channel). It is a MapNode and has the following metric:
- `Depth (m)` - L1 depth error in meters.

### 1.3 Semantic

This node is implemented under nodes/semantic.py and is a conceptual representation of a Semantic Segmentation image. The number of channels used is 13, which is an implementation detail of the dataset used (Carla) and will instead use a list of classes in the future. It is a MapNode and has the following metrics:
 - `Accuracy` - It is a global metric, so no class-balancing is done

### 1.4 Camera Normal

This node is implemented under nodes/cameranormal.py and is a conceptual representation of an Camera Normal image (3 channels). The three channels represent the orientation on X, Y and Z planes of the 3D scene w.r.t the camera position. This means it can be interpreted as the first derivative of the Depth node. It is a MapNode and has the following metrics:
- `Degrees` - The L1 error in degrees (0-360) averaged over the 3 axes (TODO: why are we doing abs(abs(y) - abs(t)) instead of just abs(y - t))

### 1.5 World Normal

This node is implemented under nodes/normal.py and is a conceptual representation of an World Normal image (3 channels). Same definitions and implementation as `CameraNormal`, however the angles are represented w.r.t the world (as if the camera were at (0, 0, 0) and looking towards (1, 0, 0), so the road is the reference plane).

### 1.6 Pose

This node is a conceptual representation of a 6-DoF absolute position (X, Y, Z, φ, θ, ψ). This node is implemented as both VectorNode (under nodes/pose.py) and MapNode (nodes/pose_dot.py) and eventually will be merged under one single file with various constructor options.

The metrics of this node are:
- `Position (m)` - The average position error in meters over the 3 translation axes
- `Orientation (degrees)` - The average orientation error in degrees over the 3 rotation axes

### 1.7 Halftone

This node is implemented under nodes/halftone.py and is a conceptual representation of an Halftone image (3 channels). It is a MapNode and has the following metrics: 
- `L1 pixel` - The L1 error represented in pixels (0 - 255) averaged over the 3 channels
- `L2`

### 1.8 Wireframe

This node is implemented under nodes/wireframe.py and is a conceptual representation of an Wireframe image (3 channels). It is a MapNode and has the following metrics: 
- `L1 pixel` - The L1 error represented in pixels (0 - 255) averaged over the 3 channels
- `L2`

### 1.9 Optical Flow

This node is implemented under nodes/opticalflow.py and is a conceptual representation of a Optical Flow (or motion field) image. This is a special node since the motion can only be described between two positions, so it can be seen as a spatio-temporal representation of a moving object in the scene (like the camera). For now the optical flow is only computed as the motion field between two adjacent `RGB` images (so no motion(Halftone(t), Wireframe(t+k) is supported yet).

It has the following constructor arguments:
- `opticalFlowMode`. It supports the following modes:
  - XY - 2 channels representing the optical flow on the X and Y axes separated
  - magnitude - 1 channel representing the magnitude of the optical flow, computed from XY as `sqrt(X**2 + y**2)`
  - XY_plus_magnitude - 3 channels representing the concatenation of the two modes above
- `opticalFlowPercentage`. Represents what percentage of the optical flow to be used as maximum value. Basically, if there is a conceptual movement on the Y axis where pixel (i, j) at time t is in the utmost left position and at time t+k is at the utmost right position, this would be a movement of +100%. However, such large flows are rare and the networks used would normalize all values around the value of 0. This value controls where we'll clip the value of the maximum allowed flow. For (3, 5) we are using values in [-3% : 3%] for X axis and [-5% : 5%] for Y axis, clipping anything else to these values.
- `opticalFlowSkip`. Represents how many frames ahead are used to compute the motion. Defaults to 1, so the flow is defined between RGB(t) and RGB(t+1). This obviously depends on the FPS & tracked object's speed of the scene as well, however that is something we can't control here.

### 1.10 Others

TODO

## 2. Models

Each edge is represented as a neural network the transforms one node concept into another. There are 4 possibilities of transformations that can be done:
- Vector2Vector (1D - 1D)
- Vector2Map (1D - 2D)
- Map2Vector (2D - 1D)
- Map2Map (2D - 2D)

Only the last 3 are implemented, under `models` submodule. The models are implemented using a encoder-decoder structure, meaning that a Map2Vector model will use a Map2Anything encoder and an Anythin2Vector decoder. However, we are only using `edge-specific` models (as described in the graph library), so each models is only used for each particular edge (no weight sharing between two RGB encoders for example).

