import torch.nn as nn
import torch.nn.functional as F
from neural_wrappers.pytorch import FeedForwardNetwork
from neural_wrappers.pytorch.utils import getNumParams

class EncoderVector2Map(FeedForwardNetwork):
	def __init__(self, dIn, numConvTransposed=6, numFilters=16):
		assert numConvTransposed > 0
		super().__init__()
		self.dIn = dIn
		self.numFilters = numFilters
		self.numConvTransposed = numConvTransposed

		self.fc1 = nn.Linear(dIn, 4 * 4 * 1024)
		self.convtr = nn.ModuleList()
		self.bn = nn.ModuleList()
		# 1024 -> 512 -> ... -> NCT-1 -> numFilters channels
		for i in range(self.numConvTransposed - 1):
			inChannels, outChannels = 1024 // (2**i), 1024 // (2**(i + 1))
			self.convtr.append(nn.ConvTranspose2d(in_channels=inChannels, out_channels=outChannels, kernel_size=2, \
				stride=2, bias=False))
			self.bn.append(nn.BatchNorm2d(num_features=outChannels))
		self.convtr.append(nn.ConvTranspose2d(in_channels=outChannels, out_channels=numFilters, kernel_size=2, \
			stride=2, bias=False))
		self.bn.append(nn.BatchNorm2d(num_features=numFilters))

	def forward(self, x):
		y = F.relu(self.fc1(x)).reshape(-1, 1024, 4, 4)
		for i in range(self.numConvTransposed):
			y = self.convtr[i](y)
			y = F.relu(self.bn[i](y))
		return y

	def __str__(self):
		return "Encoder Vector2Map. dIn: %d. NF: %d. Params: %d)" % \
			(self.dIn, self.numFilters, getNumParams(self.parameters())[1])

class DecoderVector2Map(FeedForwardNetwork):
	def __init__(self, dOut, numFilters=16):
		super().__init__()
		self.dOut = dOut
		self.numFilters = numFilters

		self.decoder = nn.ConvTranspose2d(in_channels=numFilters, out_channels=dOut, kernel_size=2, stride=2)

	def forward(self, x):
		y = self.decoder(x)
		out = y.permute(0, 2, 3, 1)
		return out

	def __str__(self):
		return "Decode Vector2Map. dOut: %d. NF: %d. Params: %d)" % \
			(self.dOut, self.numFilters, getNumParams(self.parameters())[1])
