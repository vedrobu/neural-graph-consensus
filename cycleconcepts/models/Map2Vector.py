import numpy as np
import torch.nn as nn
from neural_wrappers.pytorch import FeedForwardNetwork
from neural_wrappers.pytorch.utils import getNumParams

def conv(dIn, dOut, kernel_size):
	return nn.Sequential(
		nn.Conv2d(in_channels=dIn, out_channels=dOut, kernel_size=kernel_size, padding=(kernel_size-1)//2, stride=2),
		nn.BatchNorm2d(num_features=dOut),
		nn.ReLU(inplace=True)
	)

class EncoderMap2Vector(FeedForwardNetwork):
	def __init__(self, dIn, numFilters=256):
		super().__init__()
		self.dIn = dIn
		self.numFilters = numFilters

		conv_planes = [16, 32, 64, 128, 256, 256]
		self.conv1 = conv(dIn=dIn, dOut=conv_planes[0], kernel_size=7)
		self.conv2 = conv(dIn=conv_planes[0], dOut=conv_planes[1], kernel_size=5)
		self.conv3 = conv(dIn=conv_planes[1], dOut=conv_planes[2], kernel_size=3)
		self.conv4 = conv(dIn=conv_planes[2], dOut=conv_planes[3], kernel_size=3)
		self.conv5 = conv(dIn=conv_planes[3], dOut=conv_planes[4], kernel_size=3)
		self.conv6 = conv(dIn=conv_planes[4], dOut=conv_planes[5], kernel_size=3)
		self.conv7 = conv(dIn=conv_planes[5], dOut=numFilters, kernel_size=3)


	def forward(self, x):
		x = x.transpose(1, 3).transpose(2, 3)
		out_conv1 = self.conv1(x)
		out_conv2 = self.conv2(out_conv1)
		out_conv3 = self.conv3(out_conv2)
		out_conv4 = self.conv4(out_conv3)
		out_conv5 = self.conv5(out_conv4)
		out_conv6 = self.conv6(out_conv5)
		out_conv7 = self.conv7(out_conv6)
		return out_conv7

	def __str__(self):
		return "Encoder Map2Vector. dIn: %d. NF: %d Params: %d)" % \
			(self.dIn, self.dOut, getNumParams(self.parameters())[1])

class DecoderMap2Vector(FeedForwardNetwork):
	def __init__(self, dOut, numFilters=256):
		super().__init__()
		self.dOut = dOut
		self.numFilters = numFilters
		self.decoder1 = nn.Linear(in_features=numFilters * 4, out_features=numFilters)
		self.decoder2 = nn.Linear(in_features=numFilters, out_features=numFilters)
		self.decoder3 = nn.Linear(in_features=numFilters, out_features=dOut)

	def forward(self, x):
		x = x.reshape(x.shape[0], -1)
		y1 = self.decoder1(x)
		y2 = self.decoder2(y1)
		y3 = self.decoder3(y2)

		return y3

	def __str__(self):
		return "Decoder Map2Vector. dOut: %d. NF: %d Params: %d)" % \
			(self.dOut, self.dOut, getNumParams(self.parameters())[1])
