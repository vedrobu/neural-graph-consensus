from neural_wrappers.graph_stable import Graph

class SingleLinkGraph(Graph): pass

class TwoHopsGraph(Graph):
	def __init__(self, singleLinksPath:str, edges, hyperParameters={}):
		self.singleLinksPath = singleLinksPath
		super().__init__(edges, hyperParameters)

class GraphEnsemble(Graph):
	def __init__(self, firstLinksPath:str, edges, hyperParameters={}):
		self.firstLinksPath = firstLinksPath
		super().__init__(edges, hyperParameters)
