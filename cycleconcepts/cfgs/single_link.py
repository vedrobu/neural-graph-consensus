from neural_wrappers.graph_stable import Graph, Edge
from functools import partial
from cycleconcepts.nodes import RGB, Pose, Depth, Normal, Semantic, CameraNormal, Halftone, WireframeRegression
from cycleconcepts.models import SingleLinkGraph

def rgb2wireframeregression(hyperParameters) -> SingleLinkGraph:
	rgbNode = RGB()
	wireframeRegressionNode = WireframeRegression()
	return SingleLinkGraph([
		Edge(rgbNode, wireframeRegressionNode, blockGradients=False)
	])

def rgb2semantic(hyperParameters) -> SingleLinkGraph:
	rgbNode = RGB()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return SingleLinkGraph([
		Edge(rgbNode, semanticNode, blockGradients=False)
	])

def rgb2normal(hyperParameters) -> SingleLinkGraph:
	rgbNode = RGB()
	normalNode = Normal()
	return SingleLinkGraph([
		Edge(rgbNode, normalNode, blockGradients=False)
	])

def rgb2cameranormal(hyperParameters) -> SingleLinkGraph:
	rgbNode = RGB()
	cameranormalNode = CameraNormal()
	return SingleLinkGraph([
		Edge(rgbNode, cameranormalNode, blockGradients=False)
	])

def rgb2pose(hyperParameters) -> SingleLinkGraph:
	rgbNode = RGB()

	poseNode = Pose(**hyperParameters)
	return SingleLinkGraph([
		Edge(rgbNode, poseNode, blockGradients=False)
	])

def rgb2depth(hyperParameters) -> SingleLinkGraph:
	rgbNode = RGB()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(rgbNode, depthNode, blockGradients=False)
	])

def rgb2halftone(hyperParameters) -> SingleLinkGraph:
	rgbNode = RGB()
	halftoneNode = Halftone()
	return SingleLinkGraph([
		Edge(rgbNode, halftoneNode, blockGradients=False)
	])

def wireframeregression2rgb(hyperParameters) -> SingleLinkGraph:
	wireframeRegressionNode = WireframeRegression()
	rgbNode = RGB()
	return SingleLinkGraph([
		Edge(wireframeRegressionNode, rgbNode, blockGradients=False)
	])

def wireframeregression2semantic(hyperParameters) -> SingleLinkGraph:
	wireframeRegressionNode = WireframeRegression()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return SingleLinkGraph([
		Edge(wireframeRegressionNode, semanticNode, blockGradients=False)
	])

def wireframeregression2normal(hyperParameters) -> SingleLinkGraph:
	wireframeRegressionNode = WireframeRegression()
	normalNode = Normal()
	return SingleLinkGraph([
		Edge(wireframeRegressionNode, normalNode, blockGradients=False)
	])

def wireframeregression2cameranormal(hyperParameters) -> SingleLinkGraph:
	wireframeRegressionNode = WireframeRegression()
	cameranormalNode = CameraNormal()
	return SingleLinkGraph([
		Edge(wireframeRegressionNode, cameranormalNode, blockGradients=False)
	])

def wireframeregression2pose(hyperParameters) -> SingleLinkGraph:
	wireframeRegressionNode = WireframeRegression()
	poseNode = Pose(**hyperParameters)
	return SingleLinkGraph([
		Edge(wireframeRegressionNode, poseNode, blockGradients=False)
	])

def wireframeregression2depth(hyperParameters) -> SingleLinkGraph:
	wireframeRegressionNode = WireframeRegression()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(wireframeRegressionNode, depthNode, blockGradients=False)
	])

def wireframeregression2halftone(hyperParameters) -> SingleLinkGraph:
	wireframeRegressionNode = WireframeRegression()
	halftoneNode = Halftone()
	return SingleLinkGraph([
		Edge(wireframeRegressionNode, halftoneNode, blockGradients=False)
	])

def semantic2rgb(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	rgbNode = RGB()
	return SingleLinkGraph([
		Edge(semanticNode, rgbNode, blockGradients=False)
	])

def semantic2wireframeRegression(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	wireframeRegressionNode = WireframeRegression()
	return SingleLinkGraph([
		Edge(semanticNode, wireframeRegressionNode, blockGradients=False)
	])

def semantic2normal(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	normalNode = Normal()
	return SingleLinkGraph([
		Edge(semanticNode, normalNode, blockGradients=False)
	])

def semantic2cameranormal(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	cameranormalNode = CameraNormal()
	return SingleLinkGraph([
		Edge(semanticNode, cameranormalNode, blockGradients=False)
	])

def semantic2pose(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	poseNode = Pose(**hyperParameters)
	return SingleLinkGraph([
		Edge(semanticNode, poseNode, blockGradients=False)
	])

def semantic2depth(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(semanticNode, depthNode, blockGradients=False)
	])

def semantic2halftone(hyperParameters) -> SingleLinkGraph:
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	halftoneNode = Halftone()
	return SingleLinkGraph([
		Edge(semanticNode, halftoneNode, blockGradients=False)
	])

def normal2rgb(hyperParameters) -> SingleLinkGraph:
	normalNode = Normal()
	rgbNode = RGB()
	return SingleLinkGraph([
		Edge(normalNode, rgbNode, blockGradients=False)
	])

def normal2wireframeRegression(hyperParameters) -> SingleLinkGraph:
	normalNode = Normal()
	wireframeRegressionNode = WireframeRegression()
	return SingleLinkGraph([
		Edge(normalNode, wireframeRegressionNode, blockGradients=False)
	])

def normal2semantic(hyperParameters) -> SingleLinkGraph:
	normalNode = Normal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return SingleLinkGraph([
		Edge(normalNode, semanticNode, blockGradients=False)
	])

def normal2cameranormal(hyperParameters) -> SingleLinkGraph:
	normalNode = Normal()
	cameranormalNode = CameraNormal()
	return SingleLinkGraph([
		Edge(normalNode, cameranormalNode, blockGradients=False)
	])

def normal2pose(hyperParameters) -> SingleLinkGraph:
	normalNode = Normal()
	poseNode = Pose(**hyperParameters)
	return SingleLinkGraph([
		Edge(normalNode, poseNode, blockGradients=False)
	])

def normal2depth(hyperParameters) -> SingleLinkGraph:
	normalNode = Normal()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(normalNode, depthNode, blockGradients=False)
	])

def normal2halftone(hyperParameters) -> SingleLinkGraph:
	normalNode = Normal()
	halftoneNode = Halftone()
	return SingleLinkGraph([
		Edge(normalNode, halftoneNode, blockGradients=False)
	])

def cameranormal2rgb(hyperParameters) -> SingleLinkGraph:
	cameranormalNode = CameraNormal()
	rgbNode = RGB()
	return SingleLinkGraph([
		Edge(cameranormalNode, rgbNode, blockGradients=False)
	])

def cameranormal2wireframeRegression(hyperParameters) -> SingleLinkGraph:
	cameranormalNode = CameraNormal()
	wireframeRegressionNode = WireframeRegression()
	return SingleLinkGraph([
		Edge(cameranormalNode, wireframeRegressionNode, blockGradients=False)
	])

def cameranormal2semantic(hyperParameters) -> SingleLinkGraph:
	cameranormalNode = CameraNormal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return SingleLinkGraph([
		Edge(cameranormalNode, semanticNode, blockGradients=False)
	])

def cameranormal2normal(hyperParameters) -> SingleLinkGraph:
	cameranormalNode = CameraNormal()
	normalNode = Normal()
	return SingleLinkGraph([
		Edge(cameranormalNode, normalNode, blockGradients=False)
	])

def cameranormal2pose(hyperParameters) -> SingleLinkGraph:
	cameranormalNode = CameraNormal()
	poseNode = Pose(**hyperParameters)
	return SingleLinkGraph([
		Edge(cameranormalNode, poseNode, blockGradients=False)
	])

def cameranormal2depth(hyperParameters) -> SingleLinkGraph:
	cameranormalNode = CameraNormal()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(cameranormalNode, depthNode, blockGradients=False)
	])

def cameranormal2halftone(hyperParameters) -> SingleLinkGraph:
	cameranormalNode = CameraNormal()
	halftoneNode = Halftone()
	return SingleLinkGraph([
		Edge(cameranormalNode, halftoneNode, blockGradients=False)
	])

def pose2rgb(hyperParameters) -> SingleLinkGraph:
	poseNode = Pose(**hyperParameters)
	rgbNode = RGB()
	return SingleLinkGraph([
		Edge(poseNode, rgbNode, blockGradients=False)
	])

def pose2wireframeRegression(hyperParameters) -> SingleLinkGraph:
	poseNode = Pose(**hyperParameters)
	wireframeRegressionNode = WireframeRegression()
	return SingleLinkGraph([
		Edge(poseNode, wireframeRegressionNode, blockGradients=False)
	])

def pose2semantic(hyperParameters) -> SingleLinkGraph:
	poseNode = Pose(**hyperParameters)
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return SingleLinkGraph([
		Edge(poseNode, semanticNode, blockGradients=False)
	])

def pose2normal(hyperParameters) -> SingleLinkGraph:
	poseNode = Pose(**hyperParameters)
	normalNode = Normal()
	return SingleLinkGraph([
		Edge(poseNode, normalNode, blockGradients=False)
	])

def pose2cameranormal(hyperParameters) -> SingleLinkGraph:
	poseNode = Pose(**hyperParameters)
	cameranormalNode = CameraNormal()
	return SingleLinkGraph([
		Edge(poseNode, cameranormalNode, blockGradients=False)
	])

def pose2depth(hyperParameters) -> SingleLinkGraph:
	poseNode = Pose(**hyperParameters)
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(poseNode, depthNode, blockGradients=False)
	])

def pose2halftone(hyperParameters) -> SingleLinkGraph:
	poseNode = Pose(**hyperParameters)
	halftoneNode = Halftone()
	return SingleLinkGraph([
		Edge(poseNode, halftoneNode, blockGradients=False)
	])

def depth2rgb(hyperParameters) -> SingleLinkGraph:
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	rgbNode = RGB()
	return SingleLinkGraph([
		Edge(depthNode, rgbNode, blockGradients=False)
	])

def depth2wireframeRegression(hyperParameters) -> SingleLinkGraph:
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	wireframeRegressionNode = WireframeRegression()
	return SingleLinkGraph([
		Edge(depthNode, wireframeRegressionNode, blockGradients=False)
	])

def depth2semantic(hyperParameters) -> SingleLinkGraph:
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return SingleLinkGraph([
		Edge(depthNode, semanticNode, blockGradients=False)
	])

def depth2normal(hyperParameters) -> SingleLinkGraph:
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	normalNode = Normal()
	return SingleLinkGraph([
		Edge(depthNode, normalNode, blockGradients=False)
	])

def depth2cameranormal(hyperParameters) -> SingleLinkGraph:
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	cameranormalNode = CameraNormal()
	return SingleLinkGraph([
		Edge(depthNode, cameranormalNode, blockGradients=False)
	])

def depth2pose(hyperParameters) -> SingleLinkGraph:
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	poseNode = Pose(**hyperParameters)
	return SingleLinkGraph([
		Edge(depthNode, poseNode, blockGradients=False)
	])

def depth2halftone(hyperParameters) -> SingleLinkGraph:
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	halftoneNode = Halftone()
	return SingleLinkGraph([
		Edge(depthNode, halftoneNode, blockGradients=False)
	])

def halftone2rgb(hyperParameters) -> SingleLinkGraph:
	halftoneNode = Halftone()
	rgbNode = RGB()
	return SingleLinkGraph([
		Edge(halftoneNode, rgbNode, blockGradients=False)
	])

def halftone2wireframeRegression(hyperParameters) -> SingleLinkGraph:
	halftoneNode = Halftone()
	wireframeRegressionNode = WireframeRegression()
	return SingleLinkGraph([
		Edge(halftoneNode, wireframeRegressionNode, blockGradients=False)
	])

def halftone2semantic(hyperParameters) -> SingleLinkGraph:
	halftoneNode = Halftone()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	return SingleLinkGraph([
		Edge(halftoneNode, semanticNode, blockGradients=False)
	])

def halftone2normal(hyperParameters) -> SingleLinkGraph:
	halftoneNode = Halftone()
	normalNode = Normal()
	return SingleLinkGraph([
		Edge(halftoneNode, normalNode, blockGradients=False)
	])

def halftone2cameranormal(hyperParameters) -> SingleLinkGraph:
	halftoneNode = Halftone()
	cameranormalNode = CameraNormal()
	return SingleLinkGraph([
		Edge(halftoneNode, cameranormalNode, blockGradients=False)
	])

def halftone2pose(hyperParameters) -> SingleLinkGraph:
	halftoneNode = Halftone()
	poseNode = Pose(**hyperParameters)
	return SingleLinkGraph([
		Edge(halftoneNode, poseNode, blockGradients=False)
	])

def halftone2depth(hyperParameters) -> SingleLinkGraph:
	halftoneNode = Halftone()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	return SingleLinkGraph([
		Edge(halftoneNode, depthNode, blockGradients=False)
	])

