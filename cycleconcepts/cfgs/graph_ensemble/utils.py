import torch as tr

def meanFunction(self, x):
		if "GT" in x:
			del x["GT"]
		a = tr.cat(list(x.values())).mean(dim=0).unsqueeze(dim=0)
		return a
