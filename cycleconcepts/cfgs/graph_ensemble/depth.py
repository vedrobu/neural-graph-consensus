from typing import Dict, Any
from neural_wrappers.graph_stable import Graph, Edge, \
	forwardUseGT, forwardUseIntermediateResult, ReduceNode
from cycleconcepts.nodes import RGB, Halftone, CameraNormal, Normal, Semantic, Depth
from cycleconcepts.models import GraphEnsemble

from .utils import meanFunction

def rgb2depth_graph_ensemble(hyperParameters:Dict[str, Any], singleLinksPath:str) -> GraphEnsemble:
	rgbNode = RGB()
	halftoneNode = Halftone()
	cameraNormalNode = CameraNormal()
	normalNode = Normal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgb2depth = Edge(rgbNode, depthNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	halftone2depth = Edge(halftoneNode, depthNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True)
	semantic2depth = Edge(semanticNode, depthNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	rgb2cameranormal = Edge(rgbNode, cameraNormalNode, forwardFn=forwardUseGT, blockGradients=True)
	cameranormal2depth = Edge(cameraNormalNode, depthNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	normal2depth = Edge(normalNode, depthNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	meanDepth = ReduceNode(depthNode, forwardFn=meanFunction)

	edges = [rgb2depth, rgb2halftone, halftone2depth, rgb2semantic, semantic2depth, \
		rgb2cameranormal, cameranormal2depth, rgb2normal, normal2depth, meanDepth]
	edgeNames = ["rgb2depth", "rgb2halftone", "halftone2depth", "rgb2semantic", "semantic2depth", \
		"rgb2cameranormal", "cameranormal2depth", "rgb2normal", "normal2depth"]
	for edge, name in zip(edges[0 : -1], edgeNames):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)
	return GraphEnsemble(singleLinksPath, edges)
