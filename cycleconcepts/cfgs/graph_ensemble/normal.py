from typing import Dict, Any
from neural_wrappers.graph_stable import Graph, Edge, \
	forwardUseGT, forwardUseIntermediateResult, ReduceNode
from cycleconcepts.nodes import RGB, CameraNormal, WireframeRegression, Normal, Halftone
from cycleconcepts.models import GraphEnsemble

from .utils import meanFunction

def rgb2normal_graph_ensemble(hyperParameters:Dict[str, Any], singleLinksPath:str) -> GraphEnsemble:
	rgbNode = RGB()
	normalNode = Normal()
	wireframeNode = WireframeRegression()
	cameraNormalNode = CameraNormal()
	halftoneNode = Halftone()

	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2wireframe = Edge(rgbNode, wireframeNode, forwardFn=forwardUseGT, blockGradients=True)
	wireframe2normal = Edge(wireframeNode, normalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	rgb2cameraNormal = Edge(rgbNode, cameraNormalNode, forwardFn=forwardUseGT, blockGradients=True)
	cameraNormal2normal = Edge(cameraNormalNode, normalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	halftone2normal = Edge(halftoneNode, normalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	meanNormal = ReduceNode(normalNode, forwardFn=meanFunction)

	edges = [rgb2normal, rgb2wireframe, wireframe2normal, rgb2cameraNormal, cameraNormal2normal, \
		rgb2halftone, halftone2normal, meanNormal]
	edgeNames = ["rgb2normal", "rgb2wireframe", "wireframe2normal", "rgb2cameranormal", "cameranormal2normal", \
		"rgb2halftone", "halftone2normal"]
	for edge, name in zip(edges[0 : -1], edgeNames):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)
	return GraphEnsemble(singleLinksPath, edges)