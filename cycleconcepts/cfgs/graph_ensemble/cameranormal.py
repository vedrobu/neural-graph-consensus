from typing import Dict, Any
from neural_wrappers.graph_stable import Graph, Edge, \
	forwardUseGT, forwardUseIntermediateResult, ReduceNode
from cycleconcepts.nodes import RGB, CameraNormal, WireframeRegression, Normal
from cycleconcepts.models import GraphEnsemble

from .utils import meanFunction

def rgb2cameranormal_graph_ensemble(hyperParameters:Dict[str, Any], singleLinksPath:str) -> GraphEnsemble:
	rgbNode = RGB()
	cameraNormalNode = CameraNormal()
	normalNode = Normal()
	wireframeNode = WireframeRegression()

	rgb2cameraNormal = Edge(rgbNode, cameraNormalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	normal2cameraNormal = Edge(normalNode, cameraNormalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	rgb2wireframe = Edge(rgbNode, wireframeNode, forwardFn=forwardUseGT, blockGradients=True)
	wireframe2cameraNormal = Edge(wireframeNode, cameraNormalNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	meanWireframe = ReduceNode(wireframeNode, forwardFn=meanFunction)

	edges = [rgb2cameraNormal, rgb2normal, normal2cameraNormal, rgb2wireframe, wireframe2cameraNormal, \
        meanWireframe]
	edgeNames = ["rgb2cameranormal", "rgb2normal", "normal2cameranormal", "rgb2wireframe", "wireframe2cameranormal"]
	for edge, name in zip(edges[0 : -1], edgeNames):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)
	return GraphEnsemble(singleLinksPath, edges)
