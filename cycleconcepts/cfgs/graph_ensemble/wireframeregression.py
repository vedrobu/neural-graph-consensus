from typing import Dict, Any
from neural_wrappers.graph_stable import Graph, Edge, \
	forwardUseGT, forwardUseIntermediateResult, ReduceNode
from cycleconcepts.nodes import RGB, Halftone, WireframeRegression
from cycleconcepts.models import GraphEnsemble

from .utils import meanFunction

def rgb2wireframeregression_graph_ensemble(hyperParameters:Dict[str, Any], singleLinksPath:str) -> GraphEnsemble:
	rgbNode = RGB()
	halftoneNode = Halftone()
	wireframeNode = WireframeRegression()

	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	halftone2wireframe = Edge(halftoneNode, wireframeNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	rgb2wireframe = Edge(rgbNode, wireframeNode, forwardFn=forwardUseGT, blockGradients=True)
	wireframeMean = ReduceNode(wireframeNode, forwardFn=meanFunction)

	edges = [rgb2wireframe, rgb2halftone, halftone2wireframe, wireframeMean]
	edgeNames = ["rgb2wireframe", "rgb2halftone", "halftone2wireframe"]
	for edge, name in zip(edges[0 : -1], edgeNames):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)
	return GraphEnsemble(singleLinksPath, edges)
