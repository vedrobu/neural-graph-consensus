from typing import Dict, Any
from neural_wrappers.graph_stable import Graph, Edge, \
	forwardUseGT, forwardUseIntermediateResult, ReduceNode
from cycleconcepts.nodes import RGB, Halftone, Normal, Semantic
from cycleconcepts.models import GraphEnsemble

from .utils import meanFunction

def rgb2semantic_graph_ensemble(hyperParameters:Dict[str, Any], singleLinksPath:str) -> GraphEnsemble:
	rgbNode = RGB()
	halftoneNode = Halftone()
	normalNode = Normal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])

	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	halftone2semantic = Edge(halftoneNode, semanticNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	normal2semantic = Edge(normalNode, semanticNode, \
		forwardFn=forwardUseIntermediateResult, blockGradients=True)
	meanSemantic = ReduceNode(semanticNode, forwardFn=meanFunction)

	edges = [rgb2semantic, rgb2halftone, halftone2semantic, rgb2normal, normal2semantic, meanSemantic]
	edgeNames = ["rgb2semantic", "rgb2halftone", "halftone2semantic", "rgb2normal", "normal2semantic"]
	for edge, name in zip(edges[0 : -1], edgeNames):
		edge.loadWeights("%s/%s/model_best_Loss.pkl" % (singleLinksPath, name), yolo=True)
	return GraphEnsemble(singleLinksPath, edges)