from neural_wrappers.graph_stable import forwardUseIntermediateResult, forwardUseGT, Graph, Edge
from cycleconcepts.nodes import RGB, Depth, Semantic, Halftone, Normal, CameraNormal, WireframeRegression, Pose
from cycleconcepts.models import TwoHopsGraph

# rgb2XXX2semantic
def rgb2depth2semantic_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])

	rgb2depth = Edge(rgbNode, depthNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2depthPath = "%s/rgb2depth/model_best_Loss.pkl" % (firstLinksPath)
	rgb2depth.loadWeights(rgb2depthPath, yolo=True)
	rgb2depth.setTrainableWeights(False)
	depth2semantic = Edge(depthNode, semanticNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2depth,
		depth2semantic
	])

def rgb2halftone2semantic_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	halftoneNode = Halftone()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])

	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftonePath = "%s/rgb2halftone/model_best_Loss.pkl" % (firstLinksPath)
	rgb2halftone.loadWeights(rgb2halftonePath, yolo=True)
	rgb2halftone.setTrainableWeights(False)
	halftone2semantic = Edge(halftoneNode, semanticNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2halftone,
		halftone2semantic
	])

def rgb2normal2semantic_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	normalNode = Normal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])

	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2normalPath = "%s/rgb2normal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2normal.loadWeights(rgb2normalPath, yolo=True)
	rgb2normal.setTrainableWeights(False)
	normal2semantic = Edge(normalNode, semanticNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2normal,
		normal2semantic
	])

def rgb2wireframeregression2semantic_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	wireframeRegressionNode = WireframeRegression()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])

	rgb2wireframe = Edge(rgbNode, wireframeRegressionNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2wireframePath = "%s/rgb2wireframe/model_best_Loss.pkl" % (firstLinksPath)
	rgb2wireframe.loadWeights(rgb2wireframePath, yolo=True)
	rgb2wireframe.setTrainableWeights(False)
	wirerame2semantic = Edge(wireframeRegressionNode, semanticNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2wireframe,
		wirerame2semantic
	])

def rgb2cameranormal2semantic_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	cameranormalNode = CameraNormal()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])

	rgb2cameranormal = Edge(rgbNode, cameranormalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2cameranormalPath = "%s/rgb2cameranormal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2cameranormal.loadWeights(rgb2cameranormalPath, yolo=True)
	rgb2cameranormal.setTrainableWeights(False)
	cameranormal2semantic = Edge(cameranormalNode, semanticNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2cameranormal,
		cameranormal2semantic
	])

def rgb2pose2semantic_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])

	rgb2pose = Edge(rgbNode, poseNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2posePath = "%s/rgb2pose/model_best_Loss.pkl" % (firstLinksPath)
	rgb2pose.loadWeights(rgb2posePath, yolo=True)
	rgb2pose.setTrainableWeights(False)
	pose2semantic = Edge(poseNode, semanticNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2pose,
		pose2semantic
	])

## rgb2xxx2depth
def rgb2halftone2depth_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	halftoneNode = Halftone()
	depthNode = Depth(hyperParameters["maxDepthMeters"])

	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftonePath = "%s/rgb2halftone/model_best_Loss.pkl" % (firstLinksPath)
	rgb2halftone.loadWeights(rgb2halftonePath, yolo=True)
	rgb2halftone.setTrainableWeights(False)
	halftone2depth = Edge(halftoneNode, depthNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2halftone,
		halftone2depth
	])

def rgb2normal2depth_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	normalNode = Normal()
	depthNode = Depth(hyperParameters["maxDepthMeters"])

	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2normalPath = "%s/rgb2normal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2normal.loadWeights(rgb2normalPath, yolo=True)
	rgb2normal.setTrainableWeights(False)
	normal2depth = Edge(normalNode, depthNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2normal,
		normal2depth
	])

def rgb2cameranormal2depth_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	cameranormalNode = CameraNormal()
	depthNode = Depth(hyperParameters["maxDepthMeters"])

	rgb2cameranormal = Edge(rgbNode, cameranormalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2cameranormalPath = "%s/rgb2cameranormal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2cameranormal.loadWeights(rgb2cameranormalPath, yolo=True)
	rgb2cameranormal.setTrainableWeights(False)
	cameranormal2depth = Edge(cameranormalNode, depthNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2cameranormal,
		cameranormal2depth
	])

def rgb2semantic2depth_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2semanticPath = "%s/rgb2semantic/model_best_Loss.pkl" % (firstLinksPath)
	rgb2semantic.loadWeights(rgb2semanticPath, yolo=True)
	rgb2semantic.setTrainableWeights(False)
	semantic2Depth = Edge(semanticNode, depthNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2semantic,
		semantic2Depth
	])

def rgb2wireframeregression2depth_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	wireframeRegressionNode = WireframeRegression()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgb2wireframe = Edge(rgbNode, wireframeRegressionNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2wireframePath = "%s/rgb2wireframe/model_best_Loss.pkl" % (firstLinksPath)
	rgb2wireframe.loadWeights(rgb2wireframePath, yolo=True)
	rgb2wireframe.setTrainableWeights(False)
	wirerame2depth = Edge(wireframeRegressionNode, depthNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2wireframe,
		wirerame2depth
	])

def rgb2pose2depth_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])

	rgb2pose = Edge(rgbNode, poseNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2posePath = "%s/rgb2pose/model_best_Loss.pkl" % (firstLinksPath)
	rgb2pose.loadWeights(rgb2posePath, yolo=True)
	rgb2pose.setTrainableWeights(False)
	pose2depth = Edge(poseNode, depthNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2pose,
		pose2depth
	])

# rgb2xxx2cameranormal
def rgb2depth2cameranormal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	cameraNormalNode = CameraNormal()

	rgb2depth = Edge(rgbNode, depthNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2depthPath = "%s/rgb2depth/model_best_Loss.pkl" % (firstLinksPath)
	rgb2depth.loadWeights(rgb2depthPath, yolo=True)
	rgb2depth.setTrainableWeights(False)
	depth2cameranormal = Edge(depthNode, cameraNormalNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2depth,
		depth2cameranormal
	])

def rgb2halftone2cameranormal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	halftoneNode = Halftone()
	cameraNormalNode = CameraNormal()

	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftonePath = "%s/rgb2halftone/model_best_Loss.pkl" % (firstLinksPath)
	rgb2halftone.loadWeights(rgb2halftonePath, yolo=True)
	rgb2halftone.setTrainableWeights(False)
	halftone2cameranormal = Edge(halftoneNode, cameraNormalNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2halftone,
		halftone2cameranormal
	])

def rgb2normal2cameranormal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	normalNode = Normal()
	cameraNormalNode = CameraNormal()

	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2normalPath = "%s/rgb2normal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2normal.loadWeights(rgb2normalPath, yolo=True)
	rgb2normal.setTrainableWeights(False)
	normal2cameranormal = Edge(normalNode, cameraNormalNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2normal,
		normal2cameranormal
	])

def rgb2wireframeregression2cameranormal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	wireframeRegressionNode = WireframeRegression()
	cameraNormalNode = CameraNormal()

	rgb2wireframe = Edge(rgbNode, wireframeRegressionNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2wireframePath = "%s/rgb2wireframe/model_best_Loss.pkl" % (firstLinksPath)
	rgb2wireframe.loadWeights(rgb2wireframePath, yolo=True)
	rgb2wireframe.setTrainableWeights(False)
	wirerame2cameranormal = Edge(wireframeRegressionNode, cameraNormalNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2wireframe,
		wirerame2cameranormal
	])

def rgb2semantic2cameranormal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	cameraNormalNode = CameraNormal()

	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2semanticPath = "%s/rgb2semantic/model_best_Loss.pkl" % (firstLinksPath)
	rgb2semantic.loadWeights(rgb2semanticPath, yolo=True)
	rgb2semantic.setTrainableWeights(False)
	semantic2cameranormal = Edge(semanticNode, cameraNormalNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2semantic,
		semantic2cameranormal
	])

def rgb2pose2cameranormal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	cameraNormalNode = CameraNormal()

	rgb2pose = Edge(rgbNode, poseNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2posePath = "%s/rgb2pose/model_best_Loss.pkl" % (firstLinksPath)
	rgb2pose.loadWeights(rgb2posePath, yolo=True)
	rgb2pose.setTrainableWeights(False)
	pose2cameranormal = Edge(poseNode, cameraNormalNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2pose,
		pose2cameranormal
	])

# rgb2xxx2wireframe
def rgb2depth2wireframe_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	wireframeRegressionNode = WireframeRegression()

	rgb2depth = Edge(rgbNode, depthNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2depthPath = "%s/rgb2depth/model_best_Loss.pkl" % (firstLinksPath)
	rgb2depth.loadWeights(rgb2depthPath, yolo=True)
	rgb2depth.setTrainableWeights(False)
	depth2wireframe = Edge(depthNode, wireframeRegressionNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2depth,
		depth2wireframe
	])

def rgb2halftone2wireframe_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	halftoneNode = Halftone()
	wireframeRegressionNode = WireframeRegression()

	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftonePath = "%s/rgb2halftone/model_best_Loss.pkl" % (firstLinksPath)
	rgb2halftone.loadWeights(rgb2halftonePath, yolo=True)
	rgb2halftone.setTrainableWeights(False)
	halftone2wireframe = Edge(halftoneNode, wireframeRegressionNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2halftone,
		halftone2wireframe
	])

def rgb2normal2wireframe_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	normalNode = Normal()
	wireframeRegressionNode = WireframeRegression()

	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2normalPath = "%s/rgb2normal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2normal.loadWeights(rgb2normalPath, yolo=True)
	rgb2normal.setTrainableWeights(False)
	normal2wireframe = Edge(normalNode, wireframeRegressionNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2normal,
		normal2wireframe
	])

def rgb2cameranormal2wireframe_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	cameraNormalNode = CameraNormal()
	wireframeRegressionNode = WireframeRegression()

	rgb2cameranormal = Edge(rgbNode, cameraNormalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2cameranormalPath = "%s/rgb2cameranormal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2cameranormal.loadWeights(rgb2cameranormalPath, yolo=True)
	rgb2cameranormal.setTrainableWeights(False)
	cameranormal2wireframe = Edge(cameraNormalNode, wireframeRegressionNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2cameranormal,
		cameranormal2wireframe
	])

def rgb2semantic2wireframe_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	wireframeRegressionNode = WireframeRegression()

	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2semanticPath = "%s/rgb2semantic/model_best_Loss.pkl" % (firstLinksPath)
	rgb2semantic.loadWeights(rgb2semanticPath, yolo=True)
	rgb2semantic.setTrainableWeights(False)
	semantic2wireframe = Edge(semanticNode, wireframeRegressionNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2semantic,
		semantic2wireframe
	])

def rgb2pose2wireframe_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	wireframeRegressionNode = WireframeRegression()

	rgb2pose = Edge(rgbNode, poseNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2posePath = "%s/rgb2pose/model_best_Loss.pkl" % (firstLinksPath)
	rgb2pose.loadWeights(rgb2posePath, yolo=True)
	rgb2pose.setTrainableWeights(False)
	pose2cameranormal = Edge(poseNode, cameraNormalNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2pose,
		pose2cameranormal
	])


# rgb2XXX2pose
def rgb2depth2pose_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	rgb2depth = Edge(rgbNode, depthNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2depthPath = "%s/rgb2depth/model_best_Loss.pkl" % (firstLinksPath)
	rgb2depth.loadWeights(rgb2depthPath, yolo=True)
	rgb2depth.setTrainableWeights(False)
	depth2pose = Edge(depthNode, poseNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2depth,
		depth2pose
	])

def rgb2halftone2pose_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	halftoneNode = Halftone()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftonePath = "%s/rgb2halftone/model_best_Loss.pkl" % (firstLinksPath)
	rgb2halftone.loadWeights(rgb2halftonePath, yolo=True)
	rgb2halftone.setTrainableWeights(False)
	halftone2pose = Edge(halftoneNode, poseNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2halftone,
		halftone2pose
	])

def rgb2normal2pose_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	normalNode = Normal()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	rgb2normal = Edge(rgbNode, normalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2normalPath = "%s/rgb2normal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2normal.loadWeights(rgb2normalPath, yolo=True)
	rgb2normal.setTrainableWeights(False)
	normal2pose = Edge(normalNode, poseNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2normal,
		normal2pose
	])

def rgb2cameranormal2pose_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	cameranormalNode = CameraNormal()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	rgb2cameranormal = Edge(rgbNode, cameranormalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2cameranormalPath = "%s/rgb2cameranormal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2cameranormal.loadWeights(rgb2cameranormalPath, yolo=True)
	rgb2cameranormal.setTrainableWeights(False)
	cameranormal2pose = Edge(cameranormalNode, poseNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2cameranormal,
		cameranormal2pose
	])


def rgb2wireframeregression2pose_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	wireframeRegressionNode = WireframeRegression()
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	rgb2wireframe = Edge(rgbNode, wireframeRegressionNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2wireframePath = "%s/rgb2wireframe/model_best_Loss.pkl" % (firstLinksPath)
	rgb2wireframe.loadWeights(rgb2wireframePath, yolo=True)
	rgb2wireframe.setTrainableWeights(False)
	wireframe2pose = Edge(wireframeRegressionNode, poseNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2wireframe,
		wireframe2pose
	])

def rgb2semantic2pose_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	poseNode = Pose(positionsExtremes=hyperParameters["positionsExtremes"], poseRepresentation=hyperParameters["poseRepresentation"], outResolution=hyperParameters["resolution"], poseWeights=hyperParameters["poseWeights"], poseLossType=hyperParameters["poseLossType"])
	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2semanticPath = "%s/rgb2semantic/model_best_Loss.pkl" % (firstLinksPath)
	rgb2semantic.loadWeights(rgb2semanticPath, yolo=True)
	rgb2semantic.setTrainableWeights(False)
	semantic2pose = Edge(semanticNode, poseNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2semantic,
		semantic2pose
	])

### rgb2xxx2normal
def rgb2cameranormal2normal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	cameraNormalNode = CameraNormal()
	normalNode = Normal()

	rgb2cameranormal = Edge(rgbNode, cameraNormalNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2cameranormalPath = "%s/rgb2cameranormal/model_best_Loss.pkl" % (firstLinksPath)
	rgb2cameranormal.loadWeights(rgb2cameranormalPath, yolo=True)
	rgb2cameranormal.setTrainableWeights(False)
	cameranormal2normal = Edge(cameraNormalNode, normalNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2cameranormal,
		cameranormal2normal
	])

def rgb2halftone2normal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	halftoneNode = Halftone()
	NormalNode = Normal()

	rgb2halftone = Edge(rgbNode, halftoneNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2halftonePath = "%s/rgb2halftone/model_best_Loss.pkl" % (firstLinksPath)
	rgb2halftone.loadWeights(rgb2halftonePath, yolo=True)
	rgb2halftone.setTrainableWeights(False)
	halftone2Normal = Edge(halftoneNode, NormalNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2halftone,
		halftone2Normal
	])

def rgb2depth2normal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	depthNode = Depth(maxDepthMeters=hyperParameters["maxDepthMeters"])
	NormalNode = Normal()

	rgb2depth = Edge(rgbNode, depthNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2depthPath = "%s/rgb2depth/model_best_Loss.pkl" % (firstLinksPath)
	rgb2depth.loadWeights(rgb2depthPath, yolo=True)
	rgb2depth.setTrainableWeights(False)
	depth2Normal = Edge(depthNode, NormalNode, forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2depth,
		depth2Normal
	])

def rgb2wireframeregression2normal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	wireframeRegressionNode = WireframeRegression()
	NormalNode = Normal()

	rgb2wireframe = Edge(rgbNode, wireframeRegressionNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2wireframePath = "%s/rgb2wireframe/model_best_Loss.pkl" % (firstLinksPath)
	rgb2wireframe.loadWeights(rgb2wireframePath, yolo=True)
	rgb2wireframe.setTrainableWeights(False)
	wirerame2Normal = Edge(wireframeRegressionNode, NormalNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2wireframe,
		wirerame2Normal
	])

def rgb2semantic2normal_nogt(firstLinksPath:str, hyperParameters) -> TwoHopsGraph:
	rgbNode = RGB()
	semanticNode = Semantic(semanticNumClasses=hyperParameters["semanticNumClasses"])
	NormalNode = Normal()

	rgb2semantic = Edge(rgbNode, semanticNode, forwardFn=forwardUseGT, blockGradients=True)
	rgb2semanticPath = "%s/rgb2semantic/model_best_Loss.pkl" % (firstLinksPath)
	rgb2semantic.loadWeights(rgb2semanticPath, yolo=True)
	rgb2semantic.setTrainableWeights(False)
	semantic2Normal = Edge(semanticNode, NormalNode, \
        forwardFn=forwardUseIntermediateResult, blockGradients=True)
	return TwoHopsGraph(firstLinksPath, [
		rgb2semantic,
		semantic2Normal
	])