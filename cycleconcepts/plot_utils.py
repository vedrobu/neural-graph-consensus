import numpy as np
import cv2
from matplotlib.cm import plasma
from neural_wrappers.utilities import npGetInfo

from .nodes import *

def toImage(x):
	return x if x.shape[-1] != 1 else x[..., 0]

def getMetrics(GT, thisResult, metrics):
	# Fake a MB of 1
	GT = np.expand_dims(GT, axis=0)
	thisResult = np.expand_dims(thisResult, axis=0)
	res = {}
	for k in metrics:
		if k == "Loss" or (type(k) == tuple and k[1] == "Loss"):
			continue
		res[k] = metrics[k](GT, thisResult)
	# Stringify the reusult
	res = "\n".join(["%s: %2.3f" % (k, res[k]) for k in res])
	return res

def poseToImage(x, GT=None, node=None):
	def f(x):
		res = np.zeros((512, 512), dtype=np.float32)
		X, Y = np.int32(x[0 : 2] * 512)
		res = cv2.circle(res, center=(X, Y), radius=7, color=(1, 1, 1), thickness=13)
		return res.astype(np.uint32)
	resX = f(x)
	GT = x if GT is None else GT
	# GT is not pose, so it's Pose2XXX, thus pose is just input.
	if not (x.shape == GT.shape):
		res = np.array([resX, resX, resX], dtype=np.int32).transpose(1, 2, 0) * 255
	else:
		if not npCloseEnough(x, GT):
			# White prediction, green GT, blue intersection
			resGT = f(GT)
			resX[(resX != resGT) & (resGT == 1)] = 2
			resX[(resX == resGT) & (resGT == 1)] = 3
			res = np.zeros((512, 512, 3), dtype=np.int32)
			res[resX == 1] = [255, 255, 255]
			res[resX == 2] = [0, 0, 255]
			res[resX == 3] = [0, 255, 0]
		else:
			# GT is blue always
			res = np.zeros((512, 512, 3), dtype=np.int32)
			res[resX == 1] = [0, 0, 255]
	return res

def semanticToImage(x):
	x = np.argmax(x, axis=-1)
	# possibleColors = [(0, 0, 0), (80, 80, 80), (190, 153, 153), (250, 170, 160), (220, 20, 60), (153, 153, 153), \
	# 	(157, 234, 50), (128, 128, 128), (200, 200, 140), (107, 142, 35), (0, 0, 142), (102, 102, 156), (220, 220, 0)]
	possibleColors = [(0, 0, 0), (70, 70, 70), (153, 153, 190), (160, 170, 250), (60, 20, 220), (153, 153, 153), \
		(50, 234, 157), (128, 64, 128), (232, 35, 244), (35, 142, 107), (142, 0, 0), (156, 102, 102), (0, 220, 220)]
	possibleColors = list(map(lambda x : [x[2], x[1], x[0]], possibleColors))
	newImage = np.zeros((*x.shape, 3), dtype=np.uint8)
	for i in range(len(possibleColors)):
		newImage[x == i] = possibleColors[i]
	return newImage

def depthToImage(x):
	a = toImage(np.clip(x, 0, 1))
	b = plasma(a)[..., 0 : 3] * 255
	return b.astype(np.uint8)

def default(x):
	return (toImage(np.clip(x, 0, 1)) * 255).astype(np.uint8)

def cameraNormalToImage(x):
	x = np.abs(x)
	x = np.clip(x, 0, 1)
	# TODO: Fix this at data level!
	x[..., 2] = 1 - x[..., 2]
	x = (toImage(x) * 255).astype(np.uint8)
	return x

def normalToImage(x):
	x = np.abs(x)
	x = np.clip(x, 0, 1)
	x = (toImage(x) * 255).astype(np.uint8)
	return x

toImageFuncs = {
	RGB : lambda x, _=None, __=None : default(x),
	Depth : lambda x, _=None, __=None : depthToImage(x),
	Pose : poseToImage,
	Semantic : lambda x, _=None, __=None : semanticToImage(x),
	Normal : lambda x, _=None, __=None : normalToImage(x),
	CameraNormal : lambda x, _=None, __=None : cameraNormalToImage(x),
	WireframeRegression : lambda x, _=None, __=None : default(x),
	Halftone : lambda x, _=None, __=None : default(x)
}