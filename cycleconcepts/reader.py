import h5py
import numpy as np
from typing import Dict, Iterator, Any
from overrides import overrides
from neural_wrappers.readers import CarlaH5PathsReader, CarlaH5PathsNpyReader, \
	DatasetReader, DatasetIndex

class Reader(DatasetReader):
	def __init__(self, datasetPath, dataDims, hyperParameters):
		baseReaderType = self.getBaseReader(datasetPath)

		# We can infer how many neighbours ahead (if any) we need to use for this reader based on dataDims/hyperparams
		numNeighboursAhead = 0
		if "opticalFlowSkip" in hyperParameters:
			numNeighboursAhead = hyperParameters["opticalFlowSkip"]
		if "rgbNeighbourSkip" in hyperParameters:
			assert hyperParameters["opticalFlowSkip"] == hyperParameters["rgbNeighbourSkip"]

		if dataDims is None:
			dataDims = ["rgb", "position_dot_translation_only"]

		self.baseReader = baseReaderType(datasetPath, dataBuckets={"data" : dataDims}, 
			desiredShape=hyperParameters["resolution"], numNeighboursAhead=numNeighboursAhead,
			hyperParameters=hyperParameters)

	def getBaseReader(self, datasetPath):
		file = h5py.File(datasetPath, "r")
		item = file["train"]["rgb"][0]
		try:
			item = str(item, "utf8")
			if item.endswith("npy"):
				return CarlaH5PathsNpyReader
			else:
				return CarlaH5PathsReader
		except Exception:
			assert False, "H5 not yet supported"

	@overrides
	def getDataset(self, topLevel : str) -> Any:
		return self.baseReader.getDataset(topLevel)

	@overrides
	def getNumData(self, topLevel : str) -> int:
		return self.baseReader.getNumData(topLevel)

	@overrides
	def getBatchDatasetIndex(self, i : int, topLevel : str, batchSize : int) -> DatasetIndex:
		return self.baseReader.getBatchDatasetIndex(i, topLevel, batchSize)

	@overrides
	def iterateOneEpoch(self, topLevel : str, batchSize : int) -> Iterator[Dict[str, np.ndarray]]:
		for items in self.baseReader.iterateOneEpoch(topLevel, batchSize):
			yield items["data"], items["data"]
